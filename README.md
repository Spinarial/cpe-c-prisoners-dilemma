
# DILEMME DU PRISONNIER :video_game:

## Description du projet :clipboard:

Le doctorant a besoin d’accumuler des données expérimentales sur le dilemme du prisonnier.

Le dilemme du prisonnier caractérise en théorie des jeux une situation où deux joueurs auraient intérêt à coopérer mais où, en l'absence de communication entre les deux joueurs, chacun choisira de trahir l'autre si le jeu n'est joué qu'une fois.

La raison est que si l'un collabore et que l'autre trahit, le collaborateur est fortement pénalisé. Pourtant, si les deux joueurs trahissent, le résultat leur est moins favorable que si les deux avaient choisi de collaborer.

Le doctorant a besoin que des volontaires jouent l’un contre l’autre un nombre de fois à définir , sans jamais savoir qui sont leurs adversaires. <br>
On définira une partie comme étant un certain nombre de rounds. Un round est défini comme une confrontation trahison-collaboration entre les deux volontaires.

## Organisation du repository

Ce repository est organisé en 3 sous-dossier chacun dédié à une fonctionnalité. L'organisation complète du repository est la suivante:

```
doc
 |- client
 |    |- doxygen
 |    |- Documentation technique.md
 |    |- Documentation utilisateur.md
 |- server
 |    |- doxygen
 |    |- Documentation technique.md
 |    |- Documentation utilisateur.md
build
 |- client
 |- server
source
 |- client
 |- server
```

* `doc` est le dossier dédié à la documentation du projet. Vous y trouverez une documentation doxygen ainsi qu'une documentation technique et utilisateur.
* `build` est le dossier dédié aux dernier build effectué. Vous y trouverez des exécutables prêts à l'utilisation.
* `source` est le dossier dédié aux sources du projet. Tous les fichiers sources utilisé y sont stockés ainsi que les fichier `CMakeLists.txt` nécessaire à la compilation manuel.

## Documentation du projet

Une documentation détaillé est disponible dans le dossier `/doc` de ce repository. Celui-ci est organisé de la manière suivante:

```
doc
 |- client
 |    |- doxygen
 |    |- Documentation technique.md
 |    |- Documentation utilisateur.md
 |- server
 |    |- doxygen
 |    |- Documentation technique.md
 |    |- Documentation utilisateur.md
```

Le dossier `doxygen` est une documentation technique du code générer via l'outil [doxygen](http://doxygen.nl/). Cette documentation est aussi disponible dans le code source des deux projets "client" et "serveur". Le fichier de configuration doxygen est aussi disponible dans le code source au cas ou la documentation devrait être regénérer si besoin.

Dans chaque dossier de documentation est disponible une `Documentation technique.md` et une `Documentation utilisateur.md`. Il s'agit de deux fichiers rédigé en markdown afin de guider les utilisateurs ou les futures développeurs dans l'utilisation/modification du projet.

## Installation

### Utilisation directe

Le dossier `build` contient les deux projets pré-compilés par nos soins. Attention, il est nécessaire de télécharger le dossier complet et non uniquement l'exécutable du projet que l'on souhaite utiliser. Ceux-ci possède des fichiers supplémentaires nécessaire au bon fonctionnement du projet tel que `settings.cfg`.

### Compilation manuel

Le code source des deux projets est disponible dans le dossier `source` de ce repository. Chaque sous-dossier est un projet CMake, le fichier `CMakeLists.txt` est donc disponible pour la compilation manuelle. De plus les fichiers additionells tel que `settings.cfg` ou `glade/AppWin.glade` (pour le client) sont disponible.

#### Compiler le serveur

Pour compiler le serveur il est nécessaire d'avoir certains paquets installé nécessaire.

```
sudo apt-get install pkg-config cmake zlib1g zlib1g-dev -y
```

Une fois cela effectué, placer vous dans le dossier ou vous avez télécharger les sources et taper les commandes suivantes:
```
mkdir build
cd build
cmake -G "Unix Makefiles" ..
make
cp ../settings.cfg settings.cfg
mkdir logs
```

Démarrer le serveur

`./serveur`

#### Compiler le client

Pour compiler le client, il est nécessaire d'avoir les packages `pkg-config` `build-essential` `libgtk-3-dev`

Une fois cela effectué, placer vous dans le dossier ou vous avez télécharger les sources et taper les commandes suivantes:
```
sudo apt-get install pkg-config build-essential cmake libgtk-3-dev -y
mkdir build
cd build
cmake -G "Unix Makefiles" ..
make
cp ../settings.cfg settings.cfg
mkdir glade
cp ../glade/AppWin.glade glade/AppWin.glade
```

Démarrer le client
```
./client
```

## Outils utilisés

* [Debian](https://www.debian.org/) - Système d'exploitation
* [CMake](https://cmake.org/) - Gestionnaire de build
* [CLION](https://www.jetbrains.com/fr-fr/clion/) - IDE
* [Glade](https://glade.gnome.org/) - User interface design
* [Doxygen](http://doxygen.nl/) - Générateur de documentation

## Versions :page_facing_up:

**Dernière version :** 1.0
Liste des versions : [Cliquer pour afficher](https://gitlab.com/Spinarial/cpe-c-prisoners-dilemma)

## Auteurs :japanese_goblin: 

* **Eloi Desbrosses** _alias_ [@Spinarial](https://gitlab.com/Spinarial)
* **Franck Desfrancais** _alias_ [@Franckdsf](https://gitlab.com/Franckdsf)
* **Lucas Chanaux** _alias_ [@luc614fr](https://gitlab.com/luc614fr)
* **Lois Chabrier** _alias_ [@Lois07](https://gitlab.com/Lois07)
