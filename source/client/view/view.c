/**
 * @file
 */

#include "view.h"

int m_argc;
int **m_argv;


/**
 * @author Franck, Lois
 * @param widget
 * @param event
 * @param user_data
 * @return
 * @brief fonction lancée à la fermeture de l'application
 * envoie la deconnexion au serveur
 */
gboolean closeApp(GtkWidget *widget, GdkEvent  *event, gpointer   user_data){
    controller_disconnect();
    exit(1);
    return false;
}

/**
 * @author Franck, Lois, Lucas
 * @param button
 * @param user_data
 * @brief fonction lancée à l'appui sur le bouton trahir
 */
void joueurTrahir(GtkButton *button, gpointer user_data){
    //envoyer au controller l'action
    controller_joueurTrahir();

    //cacher les boutons d'actions
    //afficher le label "en attente du joueur"
    GtkLabel *enAtt1 = GTK_LABEL(gtk_builder_get_object(builder, "enAtt1"));
    GtkButton *butCollab = GTK_BUTTON(gtk_builder_get_object(builder, "butCollab"));
    gtk_widget_hide(button);
    gtk_widget_hide(butCollab);
    gtk_widget_show(enAtt1);
}

/**
 * @author Franck, Lois, Lucas
 * @param button
 * @param user_data
 * @brief fonction lancée à l'appui sur le bouton collab
 */
void joueurCollab(GtkButton *button, gpointer user_data){
    //envoyer au controller l'action
    controller_joueurCollab();

    //cacher les boutons d'actions
    //afficher le label "en attente du joueur"
    GtkLabel *enAtt1 = GTK_LABEL(gtk_builder_get_object(builder, "enAtt1"));
    GtkButton *butTrahir = GTK_BUTTON(gtk_builder_get_object(builder, "butTrahir"));
    gtk_widget_hide(button);
    gtk_widget_hide(butTrahir);
    gtk_widget_show(enAtt1);

}

/**
 * @author Franck, Lois
 * @brief timer de la page recap avant le lancement du prochain round (execute toutes les 1s)
 */
void recap_timer_handler(){
    //reduire de 1 le timer
    game_struct.recap_timerseconds--;
    char* txt[100];

    //update le label
    sprintf(txt, "Prochain round dans : %2is",game_struct.recap_timerseconds);
    GtkLabel *recapTimer = GTK_LABEL(gtk_builder_get_object(builder, "recap_timer"));
    gtk_label_set_text(recapTimer, txt);

    //si timer a 0
    if(game_struct.recap_timerseconds <= 0){
        //afficher la vue game
        //initialiser le prochain round
        gtk_widget_show(game_win);
        gtk_widget_hide(recap_win);
        init_round();
        game_view_init();

        //detruire ce timer
        if(recap_timeout_timer != -1)
            g_source_remove(recap_timeout_timer);
    }
}

/**
 * @author Franck, Lois
 * @param result
 * @brief Initialisation de la vue Recap
 */
void recapRound_view_init(int result){
    //init chargement label
    GtkLabel *recapTimer = GTK_LABEL(gtk_builder_get_object(builder, "recap_timer"));
    gtk_label_set_text(recapTimer, "Chargement..");

    //afficher le recap du round
    GtkLabel *recapLabel = GTK_LABEL(gtk_builder_get_object(builder, "recap"));
    char *text;
    switch(result){
        case 1:
            text = "Vous avez tous les deux collaboré.";
            break;
        case 2:
            text = "Vous vous êtes trahis mutuellement.";
            break;
        default:
            if(game_struct.playerchoice == 0) {
                text = "Vous avez collaboré, l'autre joueur vous a trahi.";
            }
            else{
                text = "Vous avez trahi l'autre joueur, il a collaboré.";
            }
            break;
    }
    gtk_label_set_text(recapLabel, text);

    //lancer le timer du recap
    if(recap_timeout_timer != -1)
        g_source_remove(recap_timeout_timer);
    recap_timeout_timer = g_timeout_add(1000, (GSourceFunc) recap_timer_handler, NULL);

    //afficher la vue recap
    //cacher les autres
    gtk_widget_hide(game_win);
    gtk_widget_hide(start_win);
    gtk_widget_show(recap_win);
}

/**
 * @author Franck
 * @brief Initialisation de la vue Recap Final
 */
void finalRecap_view_init(){
    //init variables
    GtkLabel *finalRound = GTK_LABEL(gtk_builder_get_object(builder, "Final_Round"));
    GtkLabel *finalJ1 = GTK_LABEL(gtk_builder_get_object(builder, "Final_J1"));
    GtkLabel *finalJ2 = GTK_LABEL(gtk_builder_get_object(builder, "Final_J2"));
    char *round = malloc(1000);
    char *j1 = malloc(1000);
    char *j2 = malloc(1000);

    char *choix[2] = {"collaborer", "trahir"};

    //afficher le resultat des joueurs dans
    //les labels
    for(int i =1; i <= game_struct.maxRound; i++){
        sprintf(round, "%s Round %d\n",  round, i);

        sprintf(j1, "%s%s\n",  j1, choix[results[i-1].player1_choice]);
        sprintf(j2, "%s%s\n",  j2, choix[results[i-1].player2_choice]);
    }
    gtk_label_set_text(finalRound, round);
    gtk_label_set_text(finalJ1, j1);
    gtk_label_set_text(finalJ2, j2);

    //show final recap view
    gtk_widget_hide(recap_win);
    gtk_widget_hide(game_win);
    gtk_widget_show(final_win);
}


/**
 * @author Franck, Lois, Lucas
 * @param button
 * @param user_data
 * @brief bouton de la page final recap ; bouton pour rejouer
 */
void final_restart_game(GtkButton *button, gpointer user_data){
    //disconnect de la partie
    controller_disconnect();
    gtk_widget_destroy(app_win);

    //relance une partie
    if(init_socket()){
        init_game();
        init_view(m_argc, m_argv);
    }
}

/**
 * @author Franck
 * @brief timer de la page game ; (execute toutes les 1s)
 */
void game_timer_handler(){
    //timer = timer -1
    game_struct.game_timerseconds--;
    char* txt[100];

    //afficher le timer dans le label
    GtkLabel *timelabel = GTK_LABEL(gtk_builder_get_object(builder, "timer1"));
    snprintf(txt, 100, "Temps restant : %02is", game_struct.game_timerseconds);
    gtk_label_set_text(timelabel, txt);

    //si le timer est a 0
    if(game_struct.game_timerseconds <= 0){
        //si le joueur n'a rien repondu
        //le faire collaborer
        if(game_struct.playerchoice == ACTION_NO_CHOICE){
            controller_joueurCollab();
        }
        //detruire le timer
        if(game_timeout_timer != -1)
            g_source_remove(game_timeout_timer);
    }
}

/**
 * @author Franck
 * @brief Initialisation de la vue Game
 */
void game_view_init(){
    char* txt = malloc(100);

    //init game id
    GtkLabel *gameid = GTK_LABEL(gtk_builder_get_object(builder, "game_id_num"));
    sprintf(txt, "Partie n°%d",  game_struct.id);
    gtk_label_set_text(gameid, txt);

    //init timer
    GtkLabel *timelabel = GTK_LABEL(gtk_builder_get_object(builder, "timer1"));
    gtk_label_set_text(timelabel, "Chargement..");

    //initialiser le timer du round
    if(game_timeout_timer != -1)
        g_source_remove(game_timeout_timer);
    game_timeout_timer = g_timeout_add(1000, (GSourceFunc) game_timer_handler, NULL);

    //hide en attente
    GtkLabel *enAtt1 = GTK_LABEL(gtk_builder_get_object(builder, "enAtt1"));
    gtk_widget_hide(enAtt1);

    //afficher le round actuel
    GtkLabel *round_num = GTK_LABEL(gtk_builder_get_object(builder, "round_num"));
    sprintf(txt, "Round : %d/%d",  game_struct.round, game_struct.maxRound);
    gtk_label_set_text(round_num, txt);

    //afficher les boutons d'actions
    GtkButton *butCollab = GTK_BUTTON(gtk_builder_get_object(builder, "butCollab"));
    GtkButton *butTrahir = GTK_BUTTON(gtk_builder_get_object(builder, "butTrahir"));
    gtk_widget_show(butTrahir);
    gtk_widget_show(butCollab);

    //show game view
    gtk_widget_hide(recap_win);
    gtk_widget_hide(start_win);
    gtk_widget_show(game_win);
}


/**
 * @author Franck, Lois, Lucas
 * @brief timer de la page start ; (execute toutes les 1s)
 */
void start_timer_handler(){
    //timer = timer -1
    game_struct.start_timerseconds--;
    char* txt[100];

    //afficher le timer dans son label
    GtkLabel *timelabel = GTK_LABEL(gtk_builder_get_object(builder, "start_timer"));
    snprintf(txt, 100, "%2i", game_struct.start_timerseconds);
    gtk_label_set_text(timelabel, txt);

    //si le timer est a 0
    if(game_struct.start_timerseconds <= 0){
        //cacher la page
        gtk_widget_hide(start_win);
        //initialiser la partie
        game_view_init();
        //detruire le timer
        if(start_timeout_timer != -1)
            g_source_remove(start_timeout_timer);
    }
}

/**
 * @author Franck, Lois, Lucas
 * @brief Initialisation de la vue Start
 */
void start_game_init(bool start_timer){
    char* txt[100];
    //init game id
    GtkLabel *game_id = GTK_LABEL(gtk_builder_get_object(builder, "start_game_id"));
    snprintf(txt, 100, "Partie n°%d", game_struct.id);
    gtk_label_set_text(game_id, txt);

    if(start_timer){
        //initialiser le timer d'attente
        if(start_timeout_timer != -1)
            g_source_remove(start_timeout_timer);
        start_timeout_timer = g_timeout_add(1000, (GSourceFunc) start_timer_handler, NULL);
    }
}



/**
 * @author Franck
 * @brief vue affichée lorsque l'adversaire quitte la partie en cours
 */
void leave_info_view_init(){
    //fermer la partie en cours
    gtk_widget_destroy(app_win);
    //afficher la page leave info
    gtk_init(&m_argc, &m_argv);
    builder = gtk_builder_new_from_file("glade/AppWin.glade");
    restart_win = GTK_WIDGET(gtk_builder_get_object(builder, "restart_win"));
    gtk_builder_connect_signals(builder, NULL);
    gtk_widget_show(restart_win);
    gtk_main();

}

/**
 * @author Franck
 * @param button
 * @param user_data
 * @brief bouton de la page leave info ; bouton rejouer
 */
void restart_Game(GtkButton *button, gpointer user_data){
    //disconnect
    controller_disconnect();
    gtk_widget_destroy(restart_win);

    if(init_socket()){
        init_game();
        init_view(m_argc, m_argv);
    }
}

/**
 * @author Franck, Lois
 * @param button
 * @param user_data
 * @brief bouton de la page leave info ; quitter l'application
 */
void quit_Game(GtkButton *button, gpointer user_data){
    //disconnect
    controller_disconnect();
    //quitter l'application
    exit(1);
}

/**
 * @author Franck
 * @param argc
 * @param argv
 * @brief initialiser l'application
 */
void init_view(int argc, char** argv){
    //creer la window
    gtk_init(&argc, &argv);
    builder = gtk_builder_new_from_file("glade/AppWin.glade");
    app_win = GTK_WIDGET(gtk_builder_get_object(builder, "app_win"));
    gtk_builder_connect_signals(builder, NULL);
    gtk_widget_show(app_win);

    //recuperer chaque view
    game_win = GTK_GRID(gtk_builder_get_object(builder, "Grid_App"));
    recap_win = GTK_GRID(gtk_builder_get_object(builder, "Grid_Recap"));
    start_win = GTK_GRID(gtk_builder_get_object(builder, "Grid_Start"));
    final_win = GTK_GRID(gtk_builder_get_object(builder, "Grid_Final"));

    //afficher la page start
    //(en attente d'un joueur)
    gtk_widget_hide(recap_win);
    gtk_widget_hide(game_win);
    gtk_widget_hide(final_win);
    gtk_widget_show(start_win);

    start_game_init(false);

    gtk_main();
}