/**
 * @headerfile
 */
//
// Created by serveur on 11/12/2019.
//

#ifndef C_PRISONERS_DILEMA_CLIENT_CONTROLLER_H
#define C_PRISONERS_DILEMA_CLIENT_CONTROLLER_H

#include <stdbool.h>

//structure d'une partie
struct game_struct{
    int status;
    int id;
    int playerid;
    int playerchoice;
    int round;
    int maxRound;

    int start_timerseconds;
    int game_timerseconds;
    int recap_timerseconds;
} game_struct;

//structure du recap des rounds
struct result_round{
    int player1_choice;
    int player2_choice;
    int round_result;
};

//recap de la partie
struct result_round results[100];

//fonctions
char *createMessage(int action_id);
bool init_socket();
void init_game();
void init_round();
void controller_joueurTrahir();
void controller_joueurCollab();
void controller_main(int input_protocol[]);
void controller_recapFinal();
void controller_disconnect();

#endif //C_PRISONERS_DILEMA_CLIENT_CONTROLLER_H
