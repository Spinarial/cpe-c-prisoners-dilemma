/**
 * @file
 */
#include <sys/socket.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdbool.h>

#include "socket.h"
#include "settings.h"
#include "../controller.h"

#define BUFFERSIZE 2048

/**
 *
 * @param socket_object
 * @param settings
 * @brief fonction qui initialise le socket selon les settings
 */
void socket_initialize(Socket *socket_object, Settings *settings){

    socket_object->port = atoi(settings_getOption(settings, "port"));

    // Create the socket.
    socket_object->sockfd = socket(AF_INET, SOCK_STREAM, 0);

    //Configure settings of the server address
    // Address family is Internet
    socket_object->serverAddr.sin_family = AF_INET;

    //Set port number, using htons function
    socket_object->serverAddr.sin_port = htons(socket_object->port);

    //Set IP address to localhost
    socket_object->serverAddr.sin_addr.s_addr = inet_addr(settings_getOption(settings, "server_ip"));

    //Define memory space
    memset(socket_object->serverAddr.sin_zero, '\0', sizeof socket_object->serverAddr.sin_zero);

    socket_object->status = 0;
}

/**
 *
 * @param socket
 * @param settings
 * @return
 * @brief fonction qui permet la connexion au serveur
 */
bool socket_connect(Socket *socket, Settings *settings) {
    //Connect the socket to the server using the address
    printf("Connecting to %s:%s ...\n", settings_getOption(settings, "server_ip"), settings_getOption(settings, "port"));
    if (connect(socket->sockfd, (struct sockaddr *) &socket->serverAddr, sizeof (socket->serverAddr)) != 0) {
        printf("Fail to connect to server\n");
        return false;
    };

    return true;
}

/**
 *
 * @param socket
 * @param settings
 * @return
 * @brief fonction qui crée un thread a partir du socket
 */
bool socket_start(Socket *socket, Settings *settings){

    //Creation d'un pthread en lecture
    pthread_create(&socket->thread, 0, socket_threadProcess, &socket->sockfd);

    //write(connection->sock,"Main APP Still running",15);
    pthread_detach(socket->thread);

}

/**
 *
 * @param socket
 * @param message
 * @return
 * @brief fonction d'envoi de message au serveur
 */
bool socket_write(Socket* socket, char* message){

    strcpy(socket->msg, message);
    printf("sending : %s\n", socket->msg);
    socket->status = write(socket->sockfd, socket->msg, strlen(socket->msg));

}

/**
 *
 * @param ptr
 * @return
 * @brief fonction de traitement des envois du serveur
 */
void *socket_threadProcess(void * ptr) {
    char buffer_in[BUFFERSIZE];
    int input_protocol[10];
    int socket = *((int *) ptr);
    int len;
    while ((len = read(socket, buffer_in, BUFFERSIZE)) != 0) {
        if (strncmp(buffer_in, "exit", 4) == 0) {
            break;
        }
        printf("réponse du serveur (status/game-id/player-id/action-id/timestamp) : %s\n", buffer_in);

        //Récupère le status
        char *value = strtok(buffer_in, ";");
        input_protocol[0] = atoi(value);

        /**
         * 0: Status
         * 1: Game id
         * 2: Player id
         * 3: Action id
         * 4: Timestamp
         */

        //Récupère les autres paramètre du messages
        for(int i = 1; i<= 100; i++){
            value = strtok(NULL, ";");
            if(value == NULL)
                break;

            input_protocol[i] = atoi(value);
        }

        //envoyer le resultat au controller
        //qui se chargera des actions
        controller_main(input_protocol);

    }


    close(socket);
    printf("client pthread ended, len=%d\n", len);
}







