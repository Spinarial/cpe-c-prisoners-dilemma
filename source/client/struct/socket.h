//
// Created by serveur on 08/11/2019.
//

/**
 * @headerfile
 */


#ifndef C_PRISONERS_DILEMA_CLIENT_SOCKET_H
#define C_PRISONERS_DILEMA_CLIENT_SOCKET_H

#include <stdbool.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdbool.h>

#include "settings.h"

#define SOCKET_MAX_MESSAGE_SIZE 100

#define GAME_STATUS_FIRST_CONNECTION 10
#define GAME_STATUS_NEW_GAME_WAITING 11
#define GAME_STATUS_JOINED 12
#define GAME_STATUS_PLAYING_OK 20
#define GAME_STATUS_RESULT_ROUND 22
#define GAME_STATUS_ERROR 0
#define GAME_STATUS_DISCONNECT 1
#define GAME_STATUS_OPPONENT_DISCONNECT 2
#define GAME_NO_VALUE -1
#define GAME_PLAYER_1 1
#define GAME_PLAYER_2 2
#define ACTION_BETRAY 1
#define ACTION_COLLAB 0
#define ACTION_NO_CHOICE -1



//structure du socket
typedef struct Socket {
    int sockfd;
    int status;
    char msg[SOCKET_MAX_MESSAGE_SIZE];
    pthread_t thread;

    struct sockaddr_in serverAddr;
    int port;
} Socket;

void socket_initialize(Socket *socket_object, Settings *settings);
bool socket_start(Socket *socket, Settings *settings);
bool socket_write(Socket* socket, char* message);
bool socket_connect(Socket *socket, Settings *settings);
void *socket_threadProcess(void * ptr);

#endif //C_PRISONERS_DILEMA_CLIENT_SOCKET_H
