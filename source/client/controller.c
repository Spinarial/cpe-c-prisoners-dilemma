/**
 * @file
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "controller.h"
#include "struct/settings.h"
#include "struct/socket.h"
#include "main.h"

#include <gtk-3.0/gtk/gtk.h>
#include <gtk-3.0/gtk/gtkwidget.h>
#include <time.h>
#include "view/view.h"


//envoyer l'action au serveur
/**
 * Franck, Lois, Lucas
 * @param action_id
 * @return le message construit selon le protocol
 * @brief fonction qui établie le message à envoyer au serveur
 * selon l'action ID du joueur et le protocol défini
 */
char *createActionMsg(int action_id){
    char *mesg = malloc(100);

    //create msg from protocol
    sprintf(mesg, "%d;%d;%d;%d;%lu", game_struct.status, game_struct.id, game_struct.playerid, action_id, time(NULL));

    return mesg;
}

//initialiser la connexion au serveur
/**
 * @author Lois, Eloi
 * @return true si la connexion est établie
 * @brief fonction qui etablie la connexion avec le serveur
 * selon les paramètres du setting
 */
bool init_socket(){
    bool result = false;

    //initialiser les settings
    settings_initialize(&settings, "settings.cfg");

    socket_initialize(&socket_connection, &settings);

    if(socket_connect(&socket_connection, &settings)) {
        socket_start(&socket_connection, &settings);

        return true;

    }
    return false;
}

/**
 * @author Franck, Lois
 * @brief fonction qui initialise les paramètres de la partie
 */
void init_game(){
    //si on a indiqué un gameID dans les settings
    //le définir
    if (strcmp(settings_getOption(&settings, "game_id"), "ERROR") != 0){
        game_struct.id = atoi(settings_getOption(&settings, "game_id"));
    }
    else
        game_struct.id = GAME_NO_VALUE;

    //initialiser les valeurs par défaut d'une partie
    game_struct.start_timerseconds = 4;
    game_struct.status = GAME_STATUS_FIRST_CONNECTION;
    game_struct.playerid = GAME_NO_VALUE;
    game_struct.round = 0;
    init_round();

    char *buffer_out = createActionMsg(GAME_NO_VALUE);
    socket_write(&socket_connection, buffer_out);
}

/**
 * @author Franck
 * @brief fonction d'initialisation des paramètres du round
 */
void init_round(){
    //valeurs par défaut
    //pour chaque round
    game_struct.recap_timerseconds = 3;
    game_struct.game_timerseconds = 10;
    game_struct.playerchoice = ACTION_NO_CHOICE;
}

/**
 * @author Franck
 * @brief fonction qui défini le choix de l'utilisateur et l'envoi au serveur
 */
void controller_joueurTrahir(){
    //assigner choix du joueur sur trahir
    game_struct.playerchoice = ACTION_BETRAY;
    game_struct.status = GAME_STATUS_PLAYING_OK;

    //envoyer le message au serveur
    char *buffer_out = createActionMsg(game_struct.playerchoice);
    socket_write(&socket_connection, buffer_out);
}

/**
 * @author Franck
 * @brief fonction qui défini le choix de l'utilisateur et l'envoi au serveur
 */
void controller_joueurCollab(){
    //assigner choix du joueur sur collaborer
    game_struct.playerchoice = ACTION_COLLAB;
    game_struct.status = GAME_STATUS_PLAYING_OK;

    //envoyer le message au serveur
    char *buffer_out = createActionMsg(game_struct.playerchoice);
    socket_write(&socket_connection, buffer_out);

}

/**
 * @author Franck, Lois, Lucas
 * @brief Fonction qui envoi un message de deconnexion au serveur
 */
void controller_disconnect() {
    //envoyer au serveur la deconnexion
    char *mesg = malloc(100);
    printf("Déconnexion du serveur\n");
    sprintf(mesg, "%d;%d;%d;%d;%lu", GAME_STATUS_DISCONNECT, game_struct.id, game_struct.playerid, GAME_NO_VALUE,
            time(NULL));
    socket_write(&socket_connection, mesg);
}

/**
 * @author Franck, Lois
 * @brief Fonction qui alimente le recap final du match
 */
void controller_recapRound(){
    //pour chaque round
    for(int i =0; i < game_struct.round; i++){
        //selon le resultat du round
        //remplir le tableau des choix
        switch(results[i].round_result){
            case 1:
                results[i].player1_choice = ACTION_COLLAB;
                results[i].player2_choice = ACTION_COLLAB;
                break;
            case 2:
                results[i].player1_choice = ACTION_BETRAY;
                results[i].player2_choice = ACTION_BETRAY;
                break;
            case 3:
                results[i].player1_choice = ACTION_BETRAY;
                results[i].player2_choice = ACTION_COLLAB;
                break;
            case 4:
                results[i].player1_choice = ACTION_COLLAB;
                results[i].player2_choice = ACTION_BETRAY;
                break;
        }
    }
}

/**
 * @author Franck, Lois
 * @param input_protocol
 * @brief Fonction principale gérant les messages reçus du serveur
 */
void controller_main(int input_protocol[]){
    switch (input_protocol[0]){
        //envoyer à la vue la deconnexion de l'adversaire
        case GAME_STATUS_OPPONENT_DISCONNECT :
            leave_info_view_init();
            break;
        //nouvelle partie : initialiser la game ID
        //initialiser le joueur 1
        case GAME_STATUS_NEW_GAME_WAITING :
            game_struct.id = input_protocol[1];
            game_struct.playerid = GAME_PLAYER_1;
            break;
        //les deux joueurs sont connectés
        case GAME_STATUS_JOINED :
            //si j'ai pas crée la partie
            //alors m'assigner p2
            if(game_struct.playerid == GAME_NO_VALUE){
                game_struct.id = input_protocol[1];
                game_struct.playerid = GAME_PLAYER_2;
            }
            //init les rounds
            game_struct.round = 1;
            game_struct.maxRound = input_protocol[3];

            start_game_init(true);
            break;
        //le round est terminé
        case GAME_STATUS_RESULT_ROUND:
            //ajouter au tableau récapitulatif le resultat
            //du round et lancer la fonction pour générer
            //le resultat détaillé
            results[game_struct.round -1].round_result = input_protocol[3];
            controller_recapRound();

            //si la partie est terminée
            //lancer la view recap final
            if(game_struct.round >= game_struct.maxRound){
                finalRecap_view_init();
            }
            //sinon afficher le recap du round
            else{
                recapRound_view_init(input_protocol[3]);
                game_struct.round ++;
            }
            break;
        default:
            break;


    }
}