/**
 * @file
 */

#include <gtk-3.0/gtk/gtk.h>
#include <gtk-3.0/gtk/gtkwidget.h>
#include <time.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "controller.c"
#include "view/view.c"
#include "main.h"


/**
 * @author Lucas Chanaux Franck Desfrançais Lois Chabrier
 * @param argc
 * @param argv
 * @return
 */
/**
 * @author Franck
 * @param argc
 * @param argv
 * @return
 * @brief Fonction principale qui execute le programme, initialise la connexion au serveur
 * et lance la fenêtre
 */
int main(int argc, char** argv) {
    //établier une connexion au serveur
    //si établie
    if(init_socket()){
        //init la partie
        init_game();
        init_view(argc, argv);
    }
    return (EXIT_SUCCESS);
}
