/**
 * @headerfile
 */
//
// Created by serveur on 08/11/2019.
//

#ifndef C_PRISONERS_DILEMA_CLIENT_SETTINGS_H
#define C_PRISONERS_DILEMA_CLIENT_SETTINGS_H

#include <bits/types/FILE.h>
#include <stdbool.h>

#define SETTINGS_MAX_SETTINGS 5
#define SETTINGS_LINE_SIZE 300

typedef struct Settings {
    FILE* settings_file;
    char *filename;

    char* settings_label[SETTINGS_MAX_SETTINGS];
    char* settings_value[SETTINGS_MAX_SETTINGS];
} Settings;

bool settings_initialize(Settings *setting, char *filename);
char* settings_getOption(Settings* settings, char* option);
#endif //C_PRISONERS_DILEMA_CLIENT_SETTINGS_H
