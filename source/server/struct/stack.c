/**
 * @file
 */
#include "stack.h"
#include <string.h>
#include <stdio.h>

/**
 * @author Eloi Desbrosses
 * @brief Initialize the stack object
 */
bool stack_initialize(Stack *s){

    memset(s->data, 0, GAME_MAX_GAMES);
    s->index = -1;

    return true;
}

/**
 * @author Eloi Desbrosses
 * @brief Push a value into the stack
 * @param s
 * @param value
 */
bool stack_push(Stack *s, Game value) {

    if(s->index != STACK_MAX_SIZE){
        s->data[s->index + 1] = value;
        s->index++;
    }

    return true;
}

/**
 * @author Eloi Desbrosses
 * @brief Get the first value of the stack, remove it in the process
 * @param s
 * @return Game
 */
Game stack_pop(Stack *s){
    if(s->index >= 0){
        Game result = s->data[s->index];
        s->index--;

        return result;
    }
}

/**
 * @author Eloi Desbrosses
 * @brief Test if the index is below 0, which would mean the stack is empty
 * @param s
 * @return bool
 */
bool stack_is_empty(Stack *s){
    return s->index < 0 ? true : false;
}

/**
 * @author Eloi Desbrosses
 * @brief Return the first value of the stack
 * @param s
 * @return Game
 */
Game stack_peek(Stack *s){ // aka top or front
    return s->data[s->index];
}

/**
 * @author Eloi Desbrosses
 * @brief Remove the value at the index x and rearange the stack
 * @param s
 * @param index
 * @return Game
 */
bool stack_remove(Stack *s, int index){
    if(index <= s->index){

        for(int i = index; i <= (s->index -1); i++){
           s->data[i] = s->data[i + 1];
        }

        s->index--;

        return true;
    }

    return false;
}

/**
 * @author Eloi Desbrosses
 * @brief Duplicate the first value of the stack and add it again
 * @param s
 * @return bool
 */
bool stack_dup(Stack *s){

    if(s->index != STACK_MAX_SIZE){
        s->data[s->index + 1] = s->data[s->index];
        s->index++;
    }

    return true;
}

/**
 * @autor Eloi Desbrosses
 * @brief Swap the first two values of the stack
 * @param s
 * @return bool
 */
bool stack_swap(Stack *s){

    if(s->index >= 1){
        Game temp = s->data[s->index - 1]; //value 1
        s->data[s->index - 1] = s->data[s->index];
        s->data[s->index] = temp;
    }

    return true;
}

/**
 * @author Eloi Desbrosses
 * @brief Clear the stack
 * @param s
 * @return Stack
 */
bool stack_clear(Stack *s){

    stack_initialize(s);

    return true;
}
