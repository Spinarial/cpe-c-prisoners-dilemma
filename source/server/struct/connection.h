/**
 * @headerfile
 */
//
// Created by serveur on 13/11/2019.
//

#ifndef C_PRISONERS_DILEMA_CONNECTION_H
#define C_PRISONERS_DILEMA_CONNECTION_H

#define CONNECTION_BUFFERSIZE 2048
#define CONNECTION_MAX_CLIENTS 100
#define CONNECTION_PROTOCOL_LENGTH 10

#include <sys/socket.h>
#include <netinet/in.h>
#include "settings.h"

typedef struct {
    int sockfd;
    struct sockaddr address;
    struct sockaddr_in address_in;
    int addr_len;
    int index;
    int port;
} Connection;

void connection_initialize(Connection *connection);
void connection_create_server_socket(Connection *connection, Settings *settings);

void connection_add(Connection *connection);
void connection_delete(Connection *connection);

void * connection_threadProcess(void *ptr);
void connection_write(Connection *connection, char* buffer_out);

#endif //C_PRISONERS_DILEMA_CONNECTION_H
