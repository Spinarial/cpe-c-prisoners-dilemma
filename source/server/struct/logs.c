//
// Created by serveur on 16/12/2019.
//

#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <zconf.h>
#include <time.h>
#include "logs.h"
#include "utils.h"

bool logs_initialize(Logs *logs, int game_id){

    char *filename_final = malloc(100);
    sprintf(filename_final, "logs/logs_game_%d_%lu.log", game_id, time(NULL));

    //Setup filename
    logs->filename = filename_final;

    //Setup file pointer
    logs->settings_file = fopen(filename_final, "w");

    //check if file exists
    if (logs->settings_file == NULL){
        printf("Le fichier %s n'existe pas", filename_final);
        printf("\nEchec de connexion au serveur");
        return false;
    }

    fflush(logs->settings_file);
    fclose(logs->settings_file);

    return true;
}

bool logs_write(Logs *logs, char *status, int game_id, char *string){

    logs->settings_file = fopen(logs->filename, "a");

    char *log_line = malloc(500);



//    sprintf(log_line, "%s ; %s ; Game: %d ; %s", info, status, game_id, string);
    fprintf(logs->settings_file, "%s [%s] Game: %d - %s\n", utils_get_date(), status, game_id, string);
    printf("%s [%s] Game: %d - %s\n", utils_get_date(), status, game_id, string);

    free(log_line);

    fflush(logs->settings_file);
    fclose(logs->settings_file);

    return true;
}