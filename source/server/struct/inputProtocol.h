//
// Created by serveur on 18/12/2019.
//

#ifndef C_PRISONERS_DILEMA_INPUTPROTOCOL_H
#define C_PRISONERS_DILEMA_INPUTPROTOCOL_H

#define GAME_STATUS_ERROR 0
#define GAME_STATUS_DISCONNECT 1
#define GAME_STATUS_OPPONENT_DISCONNECT 2

#define GAME_STATUS_FIRST_CONNECTION 10
#define GAME_STATUS_NEW_GAME_WAITING 11
#define GAME_STATUS_JOINED 12

#define GAME_STATUS_PLAYING_OK 20
#define GAME_STATUS_PLAYING_WAITING 21
#define GAME_STATUS_PLAYING_RESULT 22

#define GAME_NO_VALUE -1

#define GAME_PLAYER_1 1
#define GAME_PLAYER_2 2

#define ACTION_BETRAY 1
#define ACTION_COLLAB 0

#define ACTION_RESULT_BOTH_COLLAB 1
#define ACTION_RESULT_BOTH_BETRAY 2
#define ACTION_RESULT_PLAYER_1_WIN 3
#define ACTION_RESULT_PLAYER_2_WIN 4

typedef struct {
    int status;
    int game_id;
    int player_id;
    int action_id;
    long timestamp;
} InputProtocol;

#endif //C_PRISONERS_DILEMA_INPUTPROTOCOL_H
