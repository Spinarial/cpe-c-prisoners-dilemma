/**
 * @headerfile
 */
//
// Created by serveur on 27/11/2019.
//

#ifndef C_PRISONERS_DILEMA_GAME_H
#define C_PRISONERS_DILEMA_GAME_H

//Max 100 games. Or also update the stack max value
#include "connection.h"
#include "logs.h"

#define GAME_MAX_GAMES 50

typedef struct {
    int id;
    int status;
    int round_counter;
    int round_max;
    bool private;
    Logs logs;
    Connection player_1;
    int player_1_action;
    Connection player_2;
    int player_2_action;
} Game;

#endif //C_PRISONERS_DILEMA_GAME_H
