/**
 * @file
 */

#include "connection.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>

#include "settings.h"
#include "game.h"
#include "stack.h"
#include "controller.h"
#include "utils.h"

Connection* connections_array[CONNECTION_MAX_CLIENTS];

/**
 * @author Eloi Desbrosses, Aurelio Lourenço
 * @brief Initialize le controller et l'array de connection
 * @param connection
 */
void connection_initialize(Connection *connection){
    controller_initialize();

    connection->sockfd = -1;
    connection->index = 1;

    for (int i = 0; i < CONNECTION_MAX_CLIENTS; i++) {
        connections_array[i] = NULL;
    }
}

/**
 * @author Eloi Desbrosses, Aurelio Lourenço
 * @brief Créer le socket du serveur et récupère les paramètres
 * @param connection
 * @param settings
 */
void connection_create_server_socket(Connection *connection, Settings *settings) {
    connection->port = atoi(settings_getOption(settings, "port"));

    /* create socket */
    connection->sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (connection->sockfd <= 0) {
        fprintf(stderr, "%s [SERVER] ERROR - Not able to create socket\n", utils_get_date());
        exit(-3);
    }

    /* bind socket to port */
    int port = atoi(settings_getOption(settings, "port"));

    connection->address_in.sin_family = AF_INET;
    connection->address_in.sin_addr.s_addr = inet_addr(settings_getOption(settings, "ip_address"));
    connection->address_in.sin_port = htons(port);

    //bind to all ip :
    //address_in.sin_addr.s_addr = INADDR_ANY;
    //ou 0.0.0.0
    //Sinon  127.0.0.1

    /* prevent the 60 secs timeout */
    int reuse = 1;
    setsockopt(connection->sockfd, SOL_SOCKET, SO_REUSEADDR, (const char*) &reuse, sizeof (reuse));

    /* bind */
    if (bind(connection->sockfd, (struct sockaddr *) &connection->address_in, sizeof (struct sockaddr_in)) < 0) {
        fprintf(stderr, "%s [SERVER] ERROR - Not able to link the socket to the port %d\n", utils_get_date(), port);
        exit(-4);
    }
}

/**
 * @author Eloi Desbrosses
 * @brief Ajoute une connection à l'array global
 * @param connection
 */
void connection_add(Connection *connection) {
    for (int i = 0; i < CONNECTION_MAX_CLIENTS; i++) {
        if (connections_array[i] == NULL) {
            connections_array[i] = connection;
            return;
        }
    }
    printf("%s [SERVER] ERROR - Too much connetions", utils_get_date());
    exit(-5);
}

/**
 * @author Eloi Desbrosses
 * @brief Supprime une connection à l'array global
 * @param connection
 */
void connection_delete(Connection *connection) {
    for (int i = 0; i < CONNECTION_MAX_CLIENTS; i++) {
        if (connections_array[i] == connection) {
            connections_array[i] = NULL;
            return;
        }
    }
    perror("La connexion n'est pas dans le pool");
    exit(-5);
}

/**
 * @author Eloi Desbrosses, Aurelio Lourenço
 * @brief Thread allowing server to handle multiple client connections
 * @param ptr connection_t
 */
void * connection_threadProcess(void *ptr) {
    char buffer_in[CONNECTION_BUFFERSIZE];
    char buffer_out[CONNECTION_BUFFERSIZE];

    int len;
    Connection *connection;

    if (!ptr) pthread_exit(0);
    connection = (Connection *) ptr;

    connection_add(connection);

    //Welcome the new client
    printf("%s [SERVER] New connection: #%i\n", utils_get_date(), connection->index);

    int input[10];

    while ((len = read(connection->sockfd, buffer_in, CONNECTION_BUFFERSIZE)) > 0) {

//        strncat(buffer_out, buffer_in, len);

        printf("%s [SERVER] RECEIVE - %s\n", utils_get_date(), buffer_in);

        //Récupère le status
        char *value = strtok(buffer_in, ";");
        input[0] = atoi(value);

        //Récupère les autres paramètre du message
        for(int i = 1; i<= CONNECTION_PROTOCOL_LENGTH; i++){
            value = strtok(NULL, ";");
            if(value == NULL)
                break;

            input[i] = atoi(value);
        }

        InputProtocol inputProtocol = {input[0], input[1], input[2], input[3], input[4]};
        controller_main(inputProtocol, connection);

        //clear input buffer
        memset(buffer_in, '\0', CONNECTION_BUFFERSIZE);
    }

    printf("%s [SERVER] Connection %i over \n", utils_get_date(), connection->index);
    close(connection->sockfd);
    connection_delete(connection);
    free(connection);
    pthread_exit(0);
}

/**
 * @author Eloi Desbrosses
 * @brief Write a message to a socket
 * @param connection
 * @param buffer_out
 */
void connection_write(Connection *connection, char* buffer_out){
    write(connection->sockfd, buffer_out, strlen(buffer_out));
}


