/**
 * @headerfile
 */
#include <stdbool.h>
#include "game.h"

#ifndef STACK_H
#define STACK_H

#define STACK_MAX_SIZE 100

typedef struct {
    Game data[GAME_MAX_GAMES];
    int index;
} Stack;

bool stack_initialize(Stack *s);
bool stack_push(Stack *s, Game value);
Game stack_pop(Stack *s);
bool stack_is_empty(Stack *s);
Game stack_peek(Stack *s); // aka top or front
bool stack_dup(Stack *s);
bool stack_swap(Stack *s);
bool stack_remove(Stack *s, int index);
bool stack_clear(Stack *s);

#endif /* STACK_H */