//
// Created by serveur on 27/11/2019.
//

#ifndef C_PRISONERS_DILEMA_CONTROLLER_H
#define C_PRISONERS_DILEMA_CONTROLLER_H

#include "stack.h"
#include "inputProtocol.h"

void controller_initialize();

Game* controller_search_game(const int game_id);
Game* controller_create_game(int id);

void controller_main(InputProtocol inputProtocol, Connection *connection);

Game* controller_search_available_game();

bool controller_remove_game(const int game_id);

void controller_first_connection(Connection *connection, Game *selectedGame, InputProtocol inputProtocol);
void controller_first_connection_join(Connection *connection, Game *selectedGame);

void controller_disconnect(Game *selectedGame, InputProtocol inputProtocol);
void controller_game_main(Game *selectedGame, InputProtocol inputProtocol);

#endif //C_PRISONERS_DILEMA_CONTROLLER_H
