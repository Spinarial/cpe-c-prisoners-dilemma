/**
 * @file
 */
//
// Created by serveur on 08/11/2019.
//

#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "settings.h"

/**
 * @author Eloi desbrosses, Loïs Chabrier, Lucas Chanaux
 * @brief Initialize la structure settings
 * @param setting
 * @param filename
 * @return true if the settings has been initialized correctly, false otherwise
 */
bool settings_initialize(Settings *setting, char *filename){

    //Setup filename
    setting->filename = filename;

    //Setup file pointer
    setting->settings_file = fopen(filename, "r");

    //check if file exists
    if (setting->settings_file == NULL){
        printf("Le fichier %s n'existe pas", filename);
        printf("\nEchec de connexion au serveur");
        return false;
    }

    //read line by line
    char* line = malloc(SETTINGS_LINE_SIZE);
    int i = 0;

    //for each lines
    while (fgets(line, SETTINGS_LINE_SIZE, setting->settings_file) != NULL)  {

        //Ignore lines too big
        if(i == SETTINGS_MAX_SETTINGS)
            break;

        // Returns first token
        char *value = strtok(line, "=");

        setting->settings_label[i] = malloc(sizeof(value));

        strncpy (setting->settings_label[i] , value, sizeof(setting->settings_value[i]) + 2);

        //Return second token
        value = strtok(NULL, "=");
        setting->settings_value[i] = malloc(sizeof(value));

        strncpy (setting->settings_value[i] , value, sizeof(setting->settings_value[i]) + 2);

        i++;
    }

    return true;
}

/**
 * @author Eloi Desbrosses, Loïs Chabrier, Lucas Chanaux
 * @brief Get a settings by it's label
 * @param settings
 * @param option
 * @return
 */
char* settings_getOption(Settings* settings, char* option){
    for(int i = 0; i <= SETTINGS_MAX_SETTINGS; i++){

        if(settings->settings_label[i] != NULL && strcmp(settings->settings_label[i], option) == 0){
            return settings->settings_value[i];
        }
    }
    printf("error, option %s not found\n", option);
    return "ERROR";
}
