/**
 * @file
 */
//
// Created by serveur on 27/11/2019.
//

#include <zconf.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "controller.h"
#include "game.h"
#include "stack.h"
#include "connection.h"
#include "utils.h"
#include "inputProtocol.h"

Stack games_array;
Settings settings;

void controller_initialize(){
    stack_initialize(&games_array);
    //Initialize le settings
    settings_initialize(&settings, "settings.cfg");
}

//Trame server->client: status;game_id;round;action_result_id;timestamp

/**
 * @author Eloi Desbrosses
 * @brief Main function of the controller. Dispatch the actions
 * @param inputProtocol
 * @param connection
 */
void controller_main(InputProtocol inputProtocol, Connection *connection){

    Game *selectedGame;

    //Switch sur le status du message
    switch (inputProtocol.status){
        case GAME_STATUS_ERROR: //Erreur, à voir ce que l'on fait
                //TODO gérer les cas d'erreurs
            break;
        case GAME_STATUS_DISCONNECT : //Fermeture de la connection

            //Recherche de la partie à déconnecter
            selectedGame = controller_search_game(inputProtocol.game_id);

            if(selectedGame != NULL){

                controller_disconnect(selectedGame, inputProtocol);

            }else{
                printf("%s [SERVER] ERROR - No game found linked to that connection.\n", utils_get_date());
            }
            break;

        case GAME_STATUS_FIRST_CONNECTION: //Connection pour la première fois
            ;

            //Si une game id est passé dans la trame d'initialisation
            if(inputProtocol.game_id != GAME_NO_VALUE){
                selectedGame = controller_search_game(inputProtocol.game_id);
            }else{
                //Recherche d'une partie disponible à rejoindre
                selectedGame = controller_search_available_game();
            }

            //Si une partie à été fournit dans la trame d'initialisation, les fonctions s'en occuperont
            //Si aucune partie n'est disponible ont la créer et notifie le client
            if(selectedGame == NULL){
                controller_first_connection(connection, selectedGame, inputProtocol);
            }else{
                //Dans le cas ou quelqun souhaite rejoindre une game ou il y à déjà un deuxième joueur
                if(selectedGame->player_2.sockfd == 0){
                    //ont fais rejoindre la partie au joueur
                    controller_first_connection_join(connection, selectedGame);
                }else{
                    //Ont change le game id et ont lui fais créer une nouvelle game
                    inputProtocol.game_id = GAME_NO_VALUE;
                    controller_first_connection(connection, selectedGame, inputProtocol);
                }
            }

            break;
        case GAME_STATUS_PLAYING_OK: //Le jeux tourne normalement

            //Recherche de la partie concernée
            selectedGame = controller_search_game(inputProtocol.game_id);

            if(selectedGame != NULL){
                controller_game_main(selectedGame, inputProtocol);
            }else{
                printf("%s [SERVER] ERROR - Not able to find the game %d for player %d\n", utils_get_date(), inputProtocol.game_id, inputProtocol.player_id);

                char* buffer_player = malloc(100);
                sprintf(buffer_player, "%d;%d;%d;%d;%lu", GAME_STATUS_ERROR, inputProtocol.game_id, GAME_NO_VALUE, GAME_NO_VALUE, time(NULL));

                connection_write(connection, buffer_player);
            }

            break;
        default: //Comportement par défaut
            ;

            selectedGame = controller_search_game(inputProtocol.game_id);

            if(selectedGame == NULL){
                //TODO: Si la game n'existe plus pour certaines raisons, envoyer un message d'erreur au client et détruire la connexion
            }

            break;
    }
}

/**
 * @author Eloi Desbrosses
 * @brief Search for a game by it's ID
 * @param game_id
 * @return Game
 */
Game* controller_search_game(const int game_id){

    //Recherche une partie
    for(int i = 0; i<= games_array.index; i++){

        if(games_array.data[i].id == game_id){
            return &games_array.data[i];
        }

    }

    return NULL;
}

/**
 * @author Eloi Desbrosses
 * @brief Delete a game by it's ID
 * @param game_id
 * @return True if the game has been removed correctly. False otherwhise
 */
bool controller_remove_game(const int game_id){

    //Recherche une partie
    for(int i = 0; i<= games_array.index; i++){

        if(games_array.data[i].id == game_id && stack_remove(&games_array, i)){
            return true;
        }

    }
    return false;
}

/**
 * @author Eloi Desbrosses
 * @brief Search for a game available to join by it's ID
 * @param game_id
 * @return Return a Game if one is available. NULL otherwhise
 */
Game* controller_search_available_game(){

    //Recherche une partie que l'on peut rejoindre
    for(int i = 0; i<= games_array.index; i++){

        //Si la partie à le status "en attente d'un autre joueur"
        //TODO: Changer les status des games par des constantes
        if(games_array.data[i].private == false && games_array.data[i].status == GAME_STATUS_NEW_GAME_WAITING){

            //TODO: Changer les status des games par des constantes
            games_array.data[i].status = GAME_STATUS_JOINED;

            return &games_array.data[i];
        }

    }

    return NULL;
}

/**
 * @author Eloi Desbrosses
 * @brief Create a Game and return a pointer to it
 * @param inputProtocol
 * @param id
 * @param player_1
 * @return Game*
 */
Game* controller_create_game(int id){

    Game *new_game = malloc(sizeof(Game));
    new_game->id = id;
    new_game->status = GAME_STATUS_NEW_GAME_WAITING;
    new_game->round_counter = 1;
    new_game->private = false;
    new_game->round_max = atoi(settings_getOption(&settings, "rounds"));
    new_game->player_1_action = GAME_NO_VALUE;
    new_game->player_2_action = GAME_NO_VALUE;

    //initialize le fichier de log associé à cette partie
    Logs logs;
    logs_initialize(&logs, id);
    new_game->logs = logs;

    return new_game;
}

/**
 * @author Eloi Desbrosses
 * @brief Create a game for a new player if no games are available,
 * @param connection
 * @param selectedGame
 * @param inputProtocol
 */
void controller_first_connection(Connection *connection, Game *selectedGame, InputProtocol inputProtocol){

    srand(time(NULL));

    if(inputProtocol.game_id != GAME_NO_VALUE){
        selectedGame = controller_create_game(inputProtocol.game_id);
        selectedGame->private = true;
        printf("%s [SERVER] Creating new custom game with id %d\n", utils_get_date(), inputProtocol.game_id);
    }else{
        selectedGame = controller_create_game((int) time(NULL) + rand() % 100);
        printf("%s [SERVER] Creating new game with id %d\n", utils_get_date(), games_array.index + 1);
    }

    //Créer la partie et la lie au nouveau joueur
    selectedGame->player_1 = *connection;

    //Push la partie dans l'array globale
    stack_push(&games_array, *selectedGame);

    char *buffer_player_1 = malloc(100);

    sprintf(buffer_player_1, "%d;%d;1;-1;%lu", GAME_STATUS_NEW_GAME_WAITING, selectedGame->id, time(NULL));

    //Envoie des infos de base au joueur
    printf("%s [SERVER] WRITE - %s to game %d player 1\n", utils_get_date(), buffer_player_1, selectedGame->id);
    connection_write(connection, buffer_player_1);

    logs_write(&selectedGame->logs, LOGS_GAME_STATUS_NEW_GAME_WAITING, selectedGame->id, "New game created.");
}

/**
 * @author Eloi Desbrosses
 * @brief Join an available game as player 2
 * @param connection
 * @param selectedGame
 */
void controller_first_connection_join(Connection *connection, Game *selectedGame){
    //Lie la partie au joueur 2
    selectedGame->player_2 = *connection;

    //ont notifie les deux joueurs que tout est prêts
    char* buffer_player_2 = malloc(100);

    sprintf(buffer_player_2, "%d;%d;%d;%s;%lu", GAME_STATUS_JOINED, selectedGame->id, GAME_PLAYER_2, settings_getOption(&settings, "rounds"), time(NULL));

    //Préviens le joueur 2
    printf("%s [SERVER] WRITE - %s to game %d player 2\n", utils_get_date(), buffer_player_2, selectedGame->id);
    connection_write(connection, buffer_player_2);

    //Préviens le joueur 1
    printf("%s [SERVER] WRITE - %s to game %d player 1\n", utils_get_date(), buffer_player_2, selectedGame->id);
    connection_write(&selectedGame->player_1, buffer_player_2);

    selectedGame->status = GAME_STATUS_PLAYING_OK;

    logs_write(&selectedGame->logs, LOGS_GAME_STATUS_JOINED, selectedGame->id, "Game joined as player 2.");

    char *starting_round = malloc(100);
    sprintf(starting_round, "Starting round %d/%d", selectedGame->round_counter, selectedGame->round_max);
    logs_write(&selectedGame->logs, LOGS_GAME_STATUS_PLAYING_NEXT_ROUND, selectedGame->id,starting_round);
}

/**
 * @author Eloi Desbrosses
 * @param selectedGame
 * @param inputProtocol
 * @return
 */
void controller_game_main(Game *selectedGame, InputProtocol inputProtocol) {

    //Quand le joueur 1 joue, ont garde son action
    if (inputProtocol.player_id == GAME_PLAYER_1){
        selectedGame->player_1_action = inputProtocol.action_id;
        logs_write(&selectedGame->logs, LOGS_GAME_STATUS_PLAYING_OK, selectedGame->id, "Player 1 played.");
    }

    //Quand le joueur  joue, ont garde son action
    if (inputProtocol.player_id == GAME_PLAYER_2){
        selectedGame->player_2_action = inputProtocol.action_id;
        logs_write(&selectedGame->logs, LOGS_GAME_STATUS_PLAYING_OK, selectedGame->id, "Player 2 played.");
    }

    //La première fois qu'un joueur joue, ont change le status de la partie
    if (selectedGame->status == GAME_STATUS_PLAYING_OK){
        selectedGame->status = GAME_STATUS_PLAYING_WAITING;
    }
    else if (selectedGame->status == GAME_STATUS_PLAYING_WAITING && //Quand les deux joueurs ont joué, ont calcule le résultat
             (selectedGame->player_1_action != GAME_NO_VALUE && selectedGame->player_2_action != GAME_NO_VALUE)) {

        int action_result = GAME_NO_VALUE;

        char *log_result = malloc(100);
        sprintf(log_result, "Round %d/%d result:");


        //Si les deux joueurs ont collaboré, ont définis le status correspondant
        if (selectedGame->player_1_action == selectedGame->player_2_action &&
            selectedGame->player_1_action == ACTION_COLLAB) {

            action_result = ACTION_RESULT_BOTH_COLLAB;

            sprintf(log_result, "Both player collaborated");

        } else if (selectedGame->player_1_action == selectedGame->player_2_action &&
                   selectedGame->player_1_action == ACTION_BETRAY) {

            //Si les deux joueurs ont trahis, ont définis le status correspondant
            action_result = ACTION_RESULT_BOTH_BETRAY;

            sprintf(log_result, "Both player betrayed");

        } else if (selectedGame->player_1_action == ACTION_BETRAY &&
                   selectedGame->player_2_action == ACTION_COLLAB) {

            //Si le joueur 1 est le seul à trahir, il gagne
            action_result = ACTION_RESULT_PLAYER_1_WIN;

            sprintf(log_result, "Player 1 betrayed and player 2 collaborated");

        } else if (selectedGame->player_1_action == ACTION_COLLAB &&
                   selectedGame->player_2_action == ACTION_BETRAY) {

            //Si le joueur 2 est le seul à trahir, il gagne
            action_result = ACTION_RESULT_PLAYER_2_WIN;

            sprintf(log_result, "Player 1 collaborated and player 2 betrayed");
        }

        logs_write(&selectedGame->logs, LOGS_GAME_STATUS_PLAYING_RESULT, selectedGame->id,log_result);

        char *buffer_out = malloc(200);

        //ont écrit la trame de résultat
        sprintf(buffer_out, "%d;%d;%d;%d;%lu", GAME_STATUS_PLAYING_RESULT, selectedGame->id, GAME_NO_VALUE,
                action_result, time(NULL));

        //Ont envoie la trame de résultat aux deux clients
        printf("%s [SERVER] WRITE - %s to game %d player 1\n", utils_get_date(), buffer_out, selectedGame->id);
        connection_write(&selectedGame->player_1, buffer_out);
        printf("%s [SERVER] WRITE - %s to game %d player 2\n", utils_get_date(), buffer_out, selectedGame->id);
        connection_write(&selectedGame->player_2, buffer_out);

        if(selectedGame->round_counter < selectedGame->round_max){
            selectedGame->player_1_action = GAME_NO_VALUE;
            selectedGame->player_2_action = GAME_NO_VALUE;
            selectedGame->status = GAME_STATUS_PLAYING_OK;
            selectedGame->round_counter++;

            char *log_result_next_round = malloc(100);
            sprintf(log_result_next_round, "Next round %d/%d", selectedGame->round_counter, selectedGame->round_max);
            logs_write(&selectedGame->logs, LOGS_GAME_STATUS_PLAYING_NEXT_ROUND, selectedGame->id,log_result_next_round);
        }

    } else {
        //TODO: renvoyer tram erreur
    }
}

/**
 * @author Eloi Desbrosses
 * @brief Remove a game whenever one of it's player disconnect
 * @param selectedGame
 * @param inputProtocol
 */
void controller_disconnect(Game *selectedGame, InputProtocol inputProtocol){

    if(inputProtocol.player_id == GAME_PLAYER_1){ // Si c'est le joueur 1 qui se déconnecte

        if(selectedGame->player_2.sockfd != 0){ //Si il y à un deuxième joueur, ont le préviens
            char *buffer_player_2 = malloc(100);
            sprintf(buffer_player_2, "%d;%d;%d;%d;%lu", GAME_STATUS_OPPONENT_DISCONNECT, selectedGame->id, GAME_PLAYER_2, GAME_NO_VALUE, time(NULL));

            //Envoie des infos de base au joueur
            printf("%s [SERVER] WRITE - %s to game %d player 2\n", utils_get_date(), buffer_player_2, selectedGame->id);
            connection_write(&selectedGame->player_2, buffer_player_2);
        }

    }else{ //Si c'est le joueur 2 qui se déconnect, ont préviens le joueur 1
        char *buffer_player_1 = malloc(100);
        sprintf(buffer_player_1, "%d;%d;%d;%d;%lu", GAME_STATUS_OPPONENT_DISCONNECT, selectedGame->id, GAME_PLAYER_1, GAME_NO_VALUE, time(NULL));

        //Envoie des infos de base au joueur
        printf("%s [SERVER] WRITE - %s to game %d player 1\n", utils_get_date(), buffer_player_1, selectedGame->id);
        connection_write(&selectedGame->player_1, buffer_player_1);
    }

    printf("%s [SERVER] Removing game: %d\n", utils_get_date(), selectedGame->id);
    logs_write(&selectedGame->logs, LOGS_GAME_STATUS_END_GAME, selectedGame->id, "Removing game.");
    controller_remove_game(inputProtocol.game_id);
}

