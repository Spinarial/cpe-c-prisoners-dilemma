//
// Created by serveur on 16/12/2019.
//

#ifndef C_PRISONERS_DILEMA_LOGS_H
#define C_PRISONERS_DILEMA_LOGS_H

#define LOGS_GAME_STATUS_ERROR "ERROR"
#define LOGS_GAME_STATUS_DISCONNECT "DISCONNECT"
#define LOGS_GAME_STATUS_OPPONENT_DISCONNECT "OPPONENT_DISCONNECT"

#define LOGS_GAME_STATUS_FIRST_CONNECTION "INTI"
#define LOGS_GAME_STATUS_NEW_GAME_WAITING "GAME_CREATED"
#define LOGS_GAME_STATUS_JOINED "GAME_JOINED"

#define LOGS_GAME_STATUS_PLAYING_OK "PLAYING"
#define LOGS_GAME_STATUS_PLAYING_WAITING "WAITING"
#define LOGS_GAME_STATUS_PLAYING_RESULT "RESULT"
#define LOGS_GAME_STATUS_PLAYING_NEXT_ROUND "NEXT_ROUND"
#define LOGS_GAME_STATUS_END_GAME "END_GAME"

#include <bits/types/FILE.h>

typedef struct Logs {
    FILE* settings_file;
    char *filename;
} Logs;


bool logs_initialize(Logs *logs, int game_id);
bool logs_write(Logs *logs, char *status, int game_id, char *string);

#endif //C_PRISONERS_DILEMA_LOGS_H
