//
// Created by serveur on 18/12/2019.
//

#include <time.h>
#include "utils.h"

char * utils_get_date(){
    char *timestamp;
    char timestamp_temp[80];

    time_t rawtime;
    struct tm *info;

    time(&rawtime);
    info = localtime(&rawtime);
    strftime(&timestamp_temp, 80, "[%x - %I:%M:%S]", info);

    timestamp = timestamp_temp;

    return timestamp;
}