/**
 * @file
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <stdio.h>
#include <pthread.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>

#include <stdbool.h>
#include <dirent.h>
#include <utils.h>

#include "struct/settings.h"
#include "struct/connection.h"
#include "struct/game.h"
#include "struct/stack.h"

Settings settings;

/**
 * @author Eloi Desbrosses
 * @brief Main function of the programs
 * @param argc
 * @param argv
 * @return return value
 */
int main(int argc, char** argv) {

    //Initialize le settings
    settings_initialize(&settings, "settings.cfg");

    //Initialize les connections
    Connection connection;
    connection_initialize(&connection);

    pthread_t thread;

    /* create socket */
    connection_create_server_socket(&connection, &settings);

    /* listen on port , stack size 50 for incoming connections*/
    if (listen(connection.sockfd, 50) < 0) {
        fprintf(stderr, "%s [SERVER] ERROR - Not able to listen on that prot: %s\n", utils_get_date(), argv[0]);
        exit(-5);
    }

    printf("%s [SERVER] Server on\n", utils_get_date());
    printf("%s [SERVER] Listening on: %s", utils_get_date(), settings_getOption(&settings, "ip_address"));
    printf("%s [SERVER] Setting listening port to: %s\n", utils_get_date(), settings_getOption(&settings, "port"));

    //Wait for connection
    while (true) {
        /* accept incoming connections */
        Connection *connection_input = malloc(sizeof (Connection));

        connection_input->sockfd = accept(connection.sockfd, &connection.address, &connection.addr_len);
        connection_input->index++;

        if (connection_input->sockfd <= 0) {
            free(&connection_input);
        } else {
            /* start a new thread but do not wait for it */
            pthread_create(&thread, 0, connection_threadProcess, (void *) connection_input);
            pthread_detach(thread);
        }
    }
    return (EXIT_SUCCESS);

}

