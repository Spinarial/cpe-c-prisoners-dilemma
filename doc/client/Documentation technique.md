# Documentation technique - Client

## Sommaire

* Organisation du protocole
* Organisation du code
* Algorithme globale
* Interface utilisateur
* Gestion de la connexion
* Gestion de la déconnexion
* Gestion des récapitulatifs de parties


## Organisation du protocole

Le protocole mis en place à été définis comme étant une chaine de charactère composé de chiffre séparé par le charactère ";". Actuellement le protocol client->serveur est définis ainsi:

```
[STATUS];[GAME ID];[PLAYER ID];[ACTION ID];[TIMESTAMP]
```
* `STATUS`: Représente le status de la trame. Cette valeur permet de détermine l'objectif global de la trame.
* `GAME ID`: Représente l'identificateur de la partie pour laquel la trame correspond. Celle-ci peut être privé ou publique. Plus d'information sur les parties privés sont disponible dans la documentation technique du client.
* `PLAYER ID`: Représente le numéro du joueur concerné par la trame.
* `ACTION ID`: Représente l'action qu'as effecuté le joueur (Trahir ou collaborer).
* `TIMESTAMP`: Timestamp du moment précis ou la trame à été créer. Cela permet d'historiser complètement les trames et donc les actions effectués par l'utilisateur et le système tout en éliminant tout délais de connection.

Les valeurs des différents paramètres sont stockées actuellement dans le `socket.h`
<br>Le statut :
* `GAME_STATUS_FIRST_CONNECTION 10` : status représentant la première connexion d'un joueur
* `GAME_STATUS_NEW_GAME_WAITING 11` : status représentant l'attente d'un joueur
* `GAME_STATUS_JOINED 12` : status représentant la connexion d'un second joueur
* `GAME_STATUS_PLAYING_OK 20` : status représentant l'état de la partie
* `GAME_STATUS_RESULT_ROUND 22` : status contenant le resultat du round
* `GAME_STATUS_ERROR 0` : status reçu par le client si le serveur rencontre un problème
* `GAME_STATUS_DISCONNECT 1` : status envoyé lors de la déconnexion
* `GAME_STATUS_OPPONENT_DISCONNECT 2` : status reçu lorsque l'adversaire se déconnecte

Le game ID :
* `GAME_NO_VALUE -1` : valeur envoyée quand aucun game ID n'a été spécifié dans le `settings.cfg`. Le serveur place dans une partie et nous renvoi alors son game ID
* `*****` : On peut également spécifier la valeur du `settings.cfg` ou une valeur en brut pour se connecter à un game ID en particulier

Le player ID :
* `GAME_NO_VALUE -1` : valeur envoyée à la première connexion, le serveur nous renvoi le bon ID du joueur
* `GAME_PLAYER_1 1` : valeur représentant le joueur 1
* `GAME_PLAYER_2 2` : valeur représentant le joueur 2

L'action du joueur :
* `ACTION_BETRAY 1` : valeur représentant l'action de trahir
* `ACTION_COLLAB 0` : valeur représentant l'action de collaborer
* `ACTION_NO_CHOICE -1` : valeur lorsque le joueur n'a pas encore choisi

Valeur par défaut
* `GAME_NO_VALUE -1` : valeur par défaut quand aucune valeur n'est à renseigner

Il est bon de savoir que toute les valeurs ne sont pas renseigné au même moment. En effet, si une valeur (tel que l'action id) n'est pas disponible au moment de l'envoie de la trame, la valeur sera définis à `GAME_NO_VALUE`. **Il est important de toujours avoir une valeur nul aux emplacement ou aucune valeur n'est disponible**

Le protocol serveur->client reprend le protocol ci-dessus mais avec une petite variation:
```
[STATUS];[GAME ID];[PLAYER ID];[ACTION RESULT ID];[TIMESTAMP]
```
* `ACTION RESULT ID`: Représente le résultat des actions combiné de deux joueur dans une partie. il peut avoir 4 valeurs:
* `1` : Les deux joueurs ont collaborés
* `2` : Les deux joueurs ont trahis
* `3` : Le joueur 1 a trahi, le joueur 2 a collaboré
* `4` : Le joueur 1 a collaboré, le joueur 2 a trahi


## Organisation du code

Le code du client est organisé majoritairement en structures. Il existe la structure settings, socket et controller. L'organisation globale du code est la suivante:

```
client
 |- glade
 |   |-AppWin.glade
 |- struct
 |   |-settings.c
 |   |-settings.h
 |   |-socket.c
 |   |-socket.h
 |- view
 |   |-view.c
 |   |-view.h
 |- controller.c
 |- controller.h
 |- main.c
 |- main.h
 |- settings.cfg
```

Le but du projet est de reproduire une organisation MVC. La view ne joue que le rôle d'affichage, et possède les events listeners. Elle interagit avec le controller qui se charge du traitement des données. Il fait également le lien entre le socket et la vue.

### `settings.cfg`

Ce fichier permet à l'utilisateur de choisir vers quel serveur se connecter ainsi que sur quelle partie.
Pour choisir un serveur : <br>
server_ip=<i>ip du serveur</i> <br>
port=<i>port du serveur</i> <br>
Pour choisir un game ID précis : <br>
game_id=<i>id de la partie</i> <br>
<i>Supprimez la ligne game_id si vous voulez jouer contre un joueur aléatoirement</i>

### `struct`

C'est le dossier qui contient les paramètres et les fichiers qui permettent les échanges avec le serveur.

### `settings.c` & `settings.h`

Ces fichiers, situés dans `struct` permettent de faire le lien entre le fichier `settings.cfg` et l'application. Le fichier `settings.c` permet de lire le fichier puis de stocker les données dans la structure du `settings.h` et de lire cette structure.

**Constantes**

* `SETTINGS_MAX_SETTINGS 5` : qui est la taille max de settings à lire dans le settings.cfg

**Structure**

* `settings_file` : le fichier settings.cfg
* `filename` : le nom du fichier settings
* `settings_label[SETTINGS_MAX_SETTINGS]` : un tableau contenant les labels associés à leurs valeurs. sa taille dépend de la constante `SETTINGS_MAX_SETTINGS`
* `settings_value[SETTINGS_MAX_SETTINGS]` : un tableau contenant les valeurs associés à leurs labels. sa taille dépend de la constante `SETTINGS_MAX_SETTINGS`


### `socket.c` & `socket.h`

Ces fichiers sont situés dans le dossier `struct`.
Le fichier `socket.c` permet de faire le lien avec le serveur, de créer un socket, envoyer des trames au serveur et de créer un thread pour recevoir les requètes de celui-ci. 
Il permet également d'envoyer les trames du serveur vers le `controller.c`.

**Constantes**

* `BUFFERSIZE 2048` : taille maximale de la trame
* `SOCKET_MAX_MESSAGE_SIZE 100` : taille max d'un message du socket
* `GAME_STATUS_FIRST_CONNECTION 10` : status représentant la connexion d'un joueur
* `GAME_STATUS_NEW_GAME_WAITING 11` : status représentant l'attente d'un joueur
* `GAME_STATUS_JOINED 12` : status représentant la connexion d'un second joueur
* `GAME_STATUS_PLAYING_OK 20` : status représentant l'état de la partie
* `GAME_STATUS_RESULT_ROUND 22` : status contenant le resultat du round
* `GAME_STATUS_ERROR 0` :
* `GAME_STATUS_DISCONNECT 1` : status envoyé lors de la déconnexion
* `GAME_STATUS_OPPONENT_DISCONNECT 2` : status reçu lorsque l'adversaire se déconnecte
* `GAME_NO_VALUE -1` : status envoyé quand aucun game ID n'a été spécifié dans le settings.cfg
* `GAME_PLAYER_1 1` : valeur représentant le joueur 1
* `GAME_PLAYER_2 2` : valeur représentant le joueur 2
* `ACTION_BETRAY 1` : valeur représentant l'action de trahir
* `ACTION_COLLAB 0` : valeur représentant l'action de collaborer
* `ACTION_NO_CHOICE -1` : valeur lorsque le joueur n'a pas encore choisi

**Structure**

* `sockfd` : id du socket
* `status` : le status du socket
* `msg[SOCKET_MAX_MESSAGE_SIZE]` : la dernière trame reçue
* `thread` :  le thread
* `serverAddr` : l'adresse IP du socket
* `port` : le port
* 

### `controller.c` & `controller.h`

Le fichier controller se charge du traitement des données de l'application. C'est également celui que fait le lien entre l'interface graphique (`view`) et les données du serveur.
Il permet entre autre de créer un message et de l'envoyer grâce au `socket`, d'initialiser la partie et de faire les traitements nécessaires lorsqu'il reçoit la trame du serveur.

C'est la fonction createActionMsg() qui se charge de créer un message pour le serveur, suivant le protcole défini.
C'est la fonction controller_main(int input_protocol[]) qui permet de traduire les messages du serveur. Cela ce fait selon le protocole défini. Pour exemple : Une trame avec input_protocol[0] = GAME_STATUS_JOINED renverra sur la vue l'action de démarrer la partie

**Structures**

game_struct :

* `status` : le statut de la partie
* `id` : l'id de la partie
* `playerid` : l'id du joueur sur le client
* `playerchoice` : le dernier choix que le joueur a fait
* `round` : le numéro du round en cours
* `maxRound` : le maximum de round (reçu par le serveur)
* `start_timerseconds` : le timer (en secondes) avant qu'une partie commence
* `game_timerseconds` : le timer (en secondes) de la durée d'un round
* `recap_timerseconds` : le timer (en secondes) de la page récap, avant de relancer un round

recap des rounds : 

* `player1_choice`: choix du joueur 1 sur le round
* `player2_choice`: choix du joueur 2 sur le round
* `round_result`: résulat du round

structure : `results[100]` : tableau des recaps des rounds contenant tous les choix des joueurs

### `glade` & `AppWin.glade`

C'est le dossier `glade` qui contient l'interface graphique de l'application. Aujourd'hui il ne comprend qu'un fichier mais est destiné à pouvoir accueillir plusieurs interfaces.

### `view.c` & `view.h`

Le fichier `view.c` est le fichier lié au glade. Il permet de faire le lien entre le fichier glade et le code. On y retrouve le listener pour le bouton fermer l'application par exemple.
Lorsque l'utilisateur effectue une action, le fichier `view.c` reçoit l'information par un listener et le communique au controller qui effectuera le traitement des données. La view ne se charge que d'afficher les pages et les boutons.

**Variables**

* `builder` : C'est le builder permettant l'affichage des fenêtres

Windows
* `app_win` : la fenetre d'application principale
* `restart_win` : la fenetre a afficher quand le joueur adverse se déconnecte

Views
* `game_win` : la vue du jeu où les joueurs choississent de collaborer ou de trahir
* `recap_win` : la vue qui récapitule le round précédent
* `start_win` : la vue qui initialise la partie (en attente du joueur puis timer avant de lancer game_win)
* `final_win` : la vue qui récapitule tous les rounds


Les ID des timers afin de les détruire lorsqu'ils sont inutilisés.
* `game_timeout_timer` : timer du round en cours
* `start_timeout_timer` : timer avant de lancer la partie
* `recap_timeout_timer` : timer avant de lancer le prochain round


### `main.c` & `main.h`

C'est le fichier qui execute le programme. Il fait le lien entre tous les fichiers. Son rôle:

 * `Lire le settings.cfg` : Lire le settings.cfg depuis le socket avant d'initialiser la connexion avec le serveur
 * `Initialiser le socket` : Créer une connexion avec le serveur, si elle se fait, passer aux étapes suivantes
 * `Initialiser la view` : Lancer la vue et Initialiser les variables aux valeurs par défaut
 * `Initailiser le controller` : Initialiser les variables de parties aux valeurs par défaut
 

## Algorithme globale

***Graphique du lancement de l'application***

```mermaid
graph TB
    subgraph "Main"
    main_function[Main]
    end
    
    read_settings --> init_co
    main_function --> init_co
    write_msg -- numéro du joueur et de la partie --> init_game 
    subgraph "Socket"
    init_co[Initialiser le socket]
    init_co --> start_thread
    start_thread[Démarrer le thread]
    write_msg[Envoyer message au serveur]
    end
    
    init_co --> read_settings
    subgraph "Settings"
    read_settings[Lire le settings.cfg]
    end
    
    main_function --> init_game
    subgraph "Controller"
    init_game[Initialiser la partie : <br>Mettre les variables aux valeurs par défaut]
    init_game -- demander une partie --> write_msg 
    end
    
    read_glade --> init_view
    main_function --> init_view
    subgraph "View"
    init_view[Initialiser la vue : <br>Mettre les variables aux valeurs par défaut]
    init_view --> show_view
    show_view[Afficher la fenêtre de jeu]
    end
    
    init_view --> read_glade
    subgraph "Glade"
    read_glade[Lire le glade]
    end
```
<br><br>
La fonction `main` se charge `d'initialiser le socket` puis la `view` puis le `controller`.
<br><br><br>
***Graphique d'une partie vue par P1 : Le joueur collabore***

```mermaid
graph TB
    subgraph "Socket"
    thread[Thread : Ecoute le serveur]
    write_msg[Envoyer le message au serveur]
    end
    
    controller_collab --> write_msg
    subgraph "Controller"
    controller_collab[Fonction lorsque le joueur collabore]
    controller_main[Main]
    controller_writemsg[Ecrire un message via le protocole]
    controller_collab --> controller_writemsg
    controller_writemsg --> controller_collab
    end

    subgraph "View"
    view_collab[Clique sur le bouton collaborer]
    view_function --> controller_collab
    view_collab --> view_function
    view_function[Fonction du bouton collaborer]
    show_view[Modifier la fenêtre de jeu]
    end
    
    subgraph "Glade"
    read_glade[Lire le glade]
    show_view --> read_glade
    read_glade --> show_view
    view_function --> show_view
    end
```

<br><br>
L'application fonctionne globalement toujours de la même manière. Lorsqu'un utilisateur effectue une action (`view`), elle est envoyée sur le `controller`, celui-ci écrit un message via le protocole défini et l'envoi au travers du fichier `socket`.
<br><br><br>
***Graphique d'une partie vue par P2 : P1 a collaboré***

```mermaid
graph TB
    subgraph "Socket"
    thread[Thread : Ecoute le serveur]
    write_msg[Envoyer message le au serveur]
    end
    
    thread -- l'autre joueur a cliqué sur un bouton ou le temps est écoulé --> controller_main
    subgraph "Controller"
    controller_collab[Le joueur collabore]
    controller_main[Main]
    end

    controller_main --> show_view
    subgraph "View"
    view_collab[Clique sur le bouton collaborer]
    show_view[Modifier la fenêtre de jeu]
    end
    
    subgraph "Glade"
    read_glade[Lire le glade]
    show_view --> read_glade
    read_glade --> show_view
    end
```
<br><br>
Ici aussi, le fonctionnement est le même, un thread (sur le fichier `socket`) écoute en continu le serveur, lorsque celui envoi une trame, 
elle est redirigée vers le `controller` qui va s'occupe d'effectuer les traitements avant d'envoyer l'action à la `view`.
<br>

## Interface utilisateur 

L'interface utilisateur est composé de 2 fenêtres et de plusieurs sous-views:<br>
<b>App_win</b>
* `en attente d'un joueur`: premiere vue, afin d'attendre un opposant
* `partie en cours`: vue du jeu
* `récapitulatif du round`: vue récapitulatif du round
* `récapitulatif final`: vue récapitulatif de tous les rounds une fois la partie finie

<b>Restart_win</b>
* `Restart win`: vue affichée lorsque l'opposant se déconnecte

<br>

***Graphique des différentes pages du jeu***

```mermaid
graph TB
    subgraph "Main App : Fenetre affichée lors du démarrage de l'application"
    Att[En attente d'un joueur]--> Part
    Part[Partie en cours] -- S'il reste des rounds à faire -->Recap
    Recap[Récapitulatif du round] -- S'il reste des rounds à faire -->Part
    FinalRecap[Partie fini : Récapitulatif des rounds]
    
    FinalRecap -- Si appui sur rejouer --> Att
    Part -- Si tous les rounds ont été fait --> FinalRecap
    end
  
    subgraph "Restart app : Fenetre affichée lorsqu'un joueur quitte la partie"
    Leave[Si un joueur quitte]
    LeaveToo[Quitter la partie]
    Replay[Rejouer]
    
    Leave --> LeaveToo
    Leave --> Replay
    Replay --> Att
    end
```

<br><br>
## Gestion de la connexion

Lorsque l'utilisateur lance l'application, il va envoyer une trame pour demander une partie (avec ou sans Game ID), le serveur lui renvoi alors une trame avec le Game ID, et son numéro de joueur.
Si un joueur est déjà connecté, les deux joueurs reçoivent une trame pour annoncer le début de la partie.

## Gestion de la déconnexion

Lorsque l'utilisateur quitte l'application, il envoi une trame de déconnexion au serveur. L'autre joueur reçoit cette trame et va voir afficher sur sa fenêtre que son opposant s'est déconnecté.

## Gestion des récapitulatifs de parties

Après chaque round, le récapitulatif du round en cours est ajouté à un tableau dans le fichier `controller.h`, ce tableau contient le résultat de chaque round et les choix des utilisateurs. A la fin de la partie, ce tableau est affiché.