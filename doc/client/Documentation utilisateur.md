# Documentation utilisateur - CLIENT

## Sommaire

* Configuration du client via settings.cfg
* Explication du jeu 
* Explication de l'interface

## Configuration du client via settings.cfg

Afin de fonctionner correctement, le client  doit obligatoirement avoir un fichier de nom `settings.cfg` dans le même emplacement que l'exécutable de celui-ci. Nous fournissons un fichier settings.cfg de base qui est obtenable dans le dossier `build/client`.

De base, le fichier settings.cfg est composé ainsi:

```
ip_address=0.0.0.0
port=7799
game_id=4
```

* `ip_address`: Définit l'adresse ip du serveur sur lequel le client va se connecter.
* `port`: Définit le port par lequel le client va se connecter au serveur. Il est nécessaire que cela soit un port publique non utilisé par une autre application.
* `game_id`: Définit le numéro de la partie. Cela permet de jouer contre une personne de notre choix si il renseigne le même numéro de partie.

Les deux premières options sont nécessaires et doivent impérativement figurer dans le fichier settings.cfg. L'option game_id est optionelle. Si celle-ci n'est pas renseignée, l'utilisateur rejoindra une partie public joignable par le prochain utilisateur possédant la même configuration (c'est à dire sans l'option game_id dans le fichier de configuration).

## Explication du jeu 

Dans le jeu du dilemme du prisonnier, deux détenus sont emprisonnés dans des cellules séparées. La police fait à chacun des deux le même marché: <br>

"Tu as le choix entre dénoncer ton complice ou non. Si tu le dénonces et qu'il te dénonce aussi, vous aurez une remise de peine d'un an tous les deux. Si tu le dénonces et que ton complice te couvre, tu auras une remise de peine de 5 ans, mais ton complice tirera le maximum. Mais si vous vous couvrez mutuellement, vous aurez tous les deux une remise de peine de 3 ans." <br>


Ainsi cette situation, il est clair que si les deux s'entendent, ils s'en tireront globalement mieux que si l'un des deux dénonce l'autre. Mais alors l'un peut être tenté de s'en tirer encore mieux en dénonçant son complice. Craignant cela, l'autre risque aussi de dénoncer son complice pour ne pas se faire avoir. Le dilemme est donc: "faut-il accepter de couvrir son complice (donc de coopérer avec lui) ou le trahir ?"


## Explication de l'interface

Une fois le fichier configuré, le joueur peux lancer le client. Une interface s'ouvrira alors indiquant le numéro de la partie du joueur avec un message "en attente d'un joueur".<br>

Lorsqu'un deuxième joueur rejoint la partie, la partie commencera alors après un compte a rebours de 3 secondes.<br>

[Attente d'un joueur](https://imgur.com/a/ILkDQeX?fbclid=IwAR0eL67ijwQGyL8xf2jBGlPvmBf_GvGSurgtdnfDJmRjD5WH8tWu_egWRWU)


L'interface sera alors composée de deux boutons, un bouton "Trahir" et un autre bouton "Collaborer". Ces deux boutons permettent au joueur de faire son choix pour ce round.<br>

En Haut de l'interface nous avons le temps restant pour faire son choix, le numéro du round actuel ainsi que le numéro de partie.<br>

[Interface en jeu](https://imgur.com/a/bHsCc3L?fbclid=IwAR0Jkmb8asZCQnoMa85U1DSloxMX8H0dFxEry7Y41rbcuN_Kgznq9fY0T2I)

Une fois le choix effectué, il devient alors impossible de changer son choix et les deux boutons disparaissent , il faut alors attendre que l'adversaire effectue son choix ou bien que le compte a rebours atteigne sa fin pour voir le résultat du round.<br>

Le résultat du round affiche les actions des deux joueurs puis après un court temps d'attente le prochain round se lance.<br>

[Résultat du round](https://imgur.com/bz4bvR7)

Une fois tous les rounds effectués, la partie est alors terminée. La fenêtre affiche alors les résultats de tout les rounds précedent. Il est alors possible de cliquer sur le bouton rejouer pour relancer une partie. <br>

[Fin partie](https://imgur.com/IMGLiZi)

Dans le cas où notre adversaire quitte la partie en cours, un pop up s'ouvrira alors nous indiquant que l'adversaire a quitté la partie. Deux boutons seront alors disponible "Quitter" ou bien "Rejouer". "Quitter" ferme l'application tandis que "Rejouer" relance une partie.

[Adversaire qui quitte la partie](https://imgur.com/uWiAhyU)


