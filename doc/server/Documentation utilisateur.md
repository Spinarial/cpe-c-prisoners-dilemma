# Documentation utilisateur - Serveur

## Sommaire

* Configuration du serveur via settings.cfg

## Configuration du serveur via settings.cfg

Afin de fonctionner correctement, le serveur doit obligatoirement avoir un fichier de nom `settings.cfg` dans le même emplacement que l'exécutable de celui-ci. Nous fournissons un fichier settings.cfg de base qui est obtenable dans le dossier `build/server`.

De base, le fichier settings.cfg est composé ainsi:
```
ip_address=0.0.0.0
port=7799
rounds=2
```

* `ip_address`: Définit l'adresse ip sur lequel le serveur écoutera les connexions entrantes. L'adresse ip 0.0.0.0 accepte toutes les connexions tandis que 127.0.0.1 n'accepte que les connections d'un client locale.
* `port`: Définit le port sur lequel le serveur écoutera les connections entrante. Il est nécessaire que cela soit un port publique non utilisé par une autre application.
* `rounds`: Définit le nombre de round par partie. Le nombre minimum de round qui sera pris en compte est de 1.

Ces trois options sont nécessaire et doivent impérativement figuré dans le fichier settings.cfg.
