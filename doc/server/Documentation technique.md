# Documentation technique - Serveur

## Sommaire

* Organisation du code
* Organisation du protocole
* Algorithme globale
* Gestions des connections
* Gestions des parties
* Gestions des logs

## Organisation du code

Le code du serveur est organisé majoritairement en structure. Ces structures sont toutes stockés dans le dossier `struct`. L'organisation globale du code est la suivante:

```
server
 |- struct
 |   |-connection.c
 |   |-connection.h
 |   |-controller.c
 |   |-controller.h
 |   |-game.h
 |   |-inputProtocol.h
 |   |-logs.c
 |   |-logs.h
 |   |-settings.c
 |   |-settings.h
 |   |-stack.c
 |   |-stack.h
 |   |-utils.c
 |   |-utils.h
 |- main.c
```

Le but du projet est de reproduire une organisation MVC. Le model étant trop petit pour avoir sa propre partie, celui-ci a été pré-implenté dans le controller. La vue n'existant pas, la partie socket remplace celle-ci afin que la partie client fasse office de vue.

### `connection.c` & `connection.h`

La structure de connexion représente une connexion entre un client et un serveur. Pour cela nous utilisons des sockets. Toutes les connexions sont stockés dans une variable global array de connexions.

**Constantes**

* `CONNECTION_BUFFERSIZE 2048`: représente la taille totale du buffer utilisé pour la transmission de trame.
* `CONNECTION_MAX_CLIENTS 100`: représente le nombre de connections totale que l'on accepte en parralèle.
* `CONNECTION_PROTOCOL_LENGTH 10`: représente le nombre d'information indépendante récupérable au sein d'une trame.

### `controller.c` & `controller.h`

Le controller est la pièce maitresse du programme. Il effectue l'intégralité du traitement d'une connection de sont arrivé à sa destruction. Le controller gère aussi les parties en cours et la logique des règles du jeux. Le controller analyse les trames entrante, effectue le traitement de celles-ci et renvoit des trames aux clients.

Le controller n'as pas de structure ou de constante, cependant celui-ci possède une variable globale Stack qui représente toutes les parties en cours ainsi qu'une variable globale settings qui permet de récupérer la configuration du serveur en cours de route.

### `game.h`

Une partie est définis par une structure contenant:
* `id`: un id
* `status`: un status
* `round_counter`: un compteur de round
* `round_max`: un nombre de round maximum
* `is_private`: Définis si une partie est privé ou non
* `logs`: Une structure de log permettant d'historiser toute les actions de la partie
* `player_1`: Une structure Connection permettant d'identifier le joueur 1
* `player_1_action`: L'action du joueur 1
* `player_2`: Une structure de Connection permettant d'identifier le joueur 2
* `player_2_action`: L'action du joueur 2

Cette structure est utilisé dès l'authentification du joueur afin d'intéragir avec la partie de celui-ci.

### `inputProtocol.h`

Ce fichier contient la totalité des informations d'une trame ainsi que la structure représente celle-ci. Il est bon de noté qu'il ne s'agit que de la structure des trames client->serveur, en effet la trame serveur->client est différente, vous pouvez trouvé plus d'information sur celle-ci dans la documentation technique du client.

Tous les status et valeurs possible au sein de la trame sont définis via des constantes. Il est ainsi possible de comparer les informations reçut et définir les informations à envoyer simplement.

Plus d'information sur le protocol est disponible dans la partie suivante de cette documentation.

**Constantes**

* `GAME_STATUS_ERROR 0`: Définit une trame reçut comme une trame d'erreur.
* `GAME_STATUS_DISCONNECT 1`: Définit une trame reçut comme une trame de déconnexion.
* `GAME_STATUS_OPPONENT_DISCONNECT 2`: Définit une trame à envoyé comme une trame ou le joueur ennemie s'est déconnecté.

* `GAME_STATUS_FIRST_CONNECTION 10`: Définit une trame reçut comme une trame d'initialisation.
* `GAME_STATUS_NEW_GAME_WAITING 11`: Définit une trame à envoyé comme une trame d'attente d'un deuxième joueur pour démarrer une partie.
* `GAME_STATUS_JOINED 12`: Définit une trame à envoyé comme une trame ou une partie est prête et démarre.

* `GAME_STATUS_PLAYING_OK 20`: Définit une trame reçut comme une action effectué par un joueur.
* `GAME_STATUS_PLAYING_WAITING 21`: Définit une trame à envoyé comme une trame d'attente d'action d'un autre joueur.
* `GAME_STATUS_PLAYING_RESULT 22`: Définit une trame à envoyé comme une trame contenant le résultat d'un round d'une partie.

* `GAME_NO_VALUE -1`: Définit une valeur incorrect ou non existante.

* `GAME_PLAYER_1 1`: Définit la valeur identifiant le premier joueur.
* `GAME_PLAYER_2 2`: Définit la valeur identifiant le deuxième joueur.

* `ACTION_BETRAY 1`: Définit la valeur de l'action "Trahir".
* `ACTION_COLLAB 0`: Définit la valeur de l'action "Collaborer".

* `ACTION_RESULT_BOTH_COLLAB 1`: Définit la valeur du résultat ou les deux joueurs ont collaboré.
* `ACTION_RESULT_BOTH_BETRAY 2`: Définit la valeur du résultat ou les deux joueurs se sont trahi.
* `ACTION_RESULT_PLAYER_1_WIN 3`: Définit la valeur du résultat ou le joueur 1 à trahi le joueur 2 qui lui à collaborer.
* `ACTION_RESULT_PLAYER_2_WIN 4`: Définit la valeur du résultat ou le joueur 2 à trahi le joueur 1 qui lui à collaborer.

### `logs.c` & `logs.h`

La gestion des logs est effectué indépendamment du reste du projet. Chaque partie génère un fichier de log contenant comme titre son id ainsi que le timestamp de sa création. La structure des logs est la suivante:

* `settings_file`: Représente le pointeur vers le fichier de log.
* `filename`: Représente le nom du fichier, utilisé régulièrement pour actualiser le buffer du fichier.

**Constantes**

* `LOGS_GAME_STATUS_ERROR "ERROR"`
* `LOGS_GAME_STATUS_DISCONNECT "DISCONNECT"`
* `LOGS_GAME_STATUS_OPPONENT_DISCONNECT "OPPONENT_DISCONNECT"`

* `LOGS_GAME_STATUS_FIRST_CONECTION "INIT"`
* `LOGS_GAME_STATUS_NEW_GAME_WAITING "GAME_CREATED"`
* `LOGS_GAME_STATUS_JOINED "GAME_JOINED"`

* `LOGS_GAME_STATUS_PLAYING_OK "PLAYING"`
* `LOGS_GAME_STATUS_PLAYING_WAITING "WAITING"`
* `LOGS_GAME_STATUS_PLAYING_RESULT "RESULT"`
* `LOGS_GAME_STATUS_PLAYING_NEXT_ROUND "NEXT_ROUND"`
* `LOGS_GAME_STATUS_END_GAME "END_GAME"`

La totalité de ces constantes permet de définir les labels afficher dans les logs pour chaque actions effectué dans une partie.

### `settings.c` & `settings.h`

La configuration du serveur est effectué à la volé. Cependant la lecture du fichier de configuration n'est effectué qu'une seule fois lors du démarrage du serveur. Les options du serveur sont stocké dans deux tableau contenant les labels des options et les valeurs de ceux-ci. La structure des options est organisé ainsi:

* `settings_file`: Représente le pointeur vers le fichier de configuration settings.cfg.
* `filename`: Représente le nom du fichier.
* `settings_label`: Représente le tableau de labels des options.
* `settings_value`: Représente le tableau de valeurs des options.

**Constantes**

* `SETTINGS_MAX_SETTINGS 5`: Représente le nombre maximal d'option lisible dans le fichier de configuration.
* `SETTINGS_LINE_SIZE 300`: Représente le nombre de charactère lisible pour chaque ligne du fichier de configuration.

### `stack.c` & `stack.h`

La structure de la pile (stack) permet de gérer la totalité des partie de manière simplifié. La stack est une strucutre pré-codé qui à juste été importé pour réutiliser du code déja existant.

**Constantes**

* `STACK_MAX_SIZE 100`: Définit la taille maximal de partie qeu la stack peut contenir.

### `utils.c` & `utils.h`

Ces fichiers utilitaires permettent de stocker des fonctionnalité nécessaire dans la globalité du projet.

## Organisation du protocole

Le protocole mis en place à été définis comme étant une chaine de charactère composé de chiffre séparé par le charactère ";". Actuellement le protocol client->serveur est définis ainsi:

```
[STATUS];[GAME ID];[PLAYER ID];[ACTION ID];[TIMESTAMP]
```
* `STATUS`: Représente le status de la trame. Cette valeur permet de détermine l'objectif global de la trame.
* `GAME ID`: Représente l'identificateur de la partie pour laquel la trame correspond. Celle-ci peut être privé ou publique. Plus d'information sur les parties privés sont disponible dans la documentation technique du client.
* `PLAYER ID`: Représente le numéro du joueur concerné par la trame.
* `ACTION ID`: Représente l'action qu'as effecuté le joueur (Trahir ou collaborer).
* `TIMESTAMP`: Timestamp du moment précis ou la trame à été créer. Cela permet d'historiser complètement les trames et donc les actions effectués par l'utilisateur et le système tout en éliminant tout délais de connexion.

Le protocol serveur->client reprend le protocol ci-dessus mais avec une petite variation:

```
[STATUS];[GAME ID];[PLAYER ID];[ACTION RESULT ID];[TIMESTAMP]
```
* `ACTION RESULT ID`: Représente le résultat des actions combiné de deux joueur dans une partie.

Il est bon de savoir que toute les valeurs ne sont pas renseigné au même moment. En effet, si une valeur (tel que l'action id) n'est pas disponible au moment de l'envoie de la trame, la valeur sera définis à `GAME_NO_VALUE`. **Il est important de toujours avoir une valeur nul aux emplacement ou aucune valeur n'est disponible**

## Algorithme globale

Les algorithmes ci-dessous résume la totalité du fonctionnement du serveur de manière abrégé. Cela permet d'accélérer le processus d'analyse en cas de modification.
Ces graphiques sont séparer en plusieurs partie pour des raisons de lisibilité.

***Graphique des connexions***
```mermaid
graph TD;
    beginning{{Début}} --> server_start(Démarrage du serveur)
    server_start --> setup_settings(Initialise le paramétrage du serveur.<br />Initialise les Connection possibles. <br />Initialise le socket.)

    setup_settings --> waiting_for_connections(Attend une connection entrante)
    A((A)) --> waiting_for_connections
    waiting_for_connections -->|Dès qu'il y à une connection entrante| start_connection_thread(Démarre un nouveau thread pour cette connection. <br />Ajoute la nouvelle Connection à la liste des Connections globale.)

    start_connection_thread --> read_protocol(Dès qu'une trame est reçut, la lie)
    E((E)) --> read_protocol
    wait_for_protocol(Attend la prochaine trame) --> read_protocol
    read_protocol --> is_connection_over{La connection est-elle terminé ?}
    is_connection_over -->|Oui| free_memory_and_close_thread(Libère la mémoire de la connection et ferme celle-ci) --> waiting_for_connections
    is_connection_over -->|Non| setup_input_protocol(Définit une nouvelle structure inputProtocol avec la trame reçut.)
    setup_input_protocol --> B((B))
```

***Graphique du controlleur***
```mermaid
graph TD;
    B((B)) --> controller_main{Qu'elle est le <br />status de la trame ?}

    controller_main -->|GAME_STATUS_ERROR| free_memory_and_close_thread(Libère la mémoire de la connection et ferme celle-ci) --> A((A))

    controller_main -->|GAME_STATUS_DISCONNECT| controller_destroy_game(Envoie une trame <br />GAME_STATUS_OPPONENT_DISCONNECT <br />à l'autre joueur si il existe <br />puis détruit la partie) --> free_memory_and_close_thread

    controller_main -->|GAME_STATUS_FIRST_CONNECTION| pass_input_protocol_structure_first_connection(Passe la structure inputProtocol à l'algorithme d'initialiasation de connection) --> C((C))

    controller_main -->|GAME_STATUS_PLAYING_OK| pass_input_protocol_structure_playing(Passe la structure inputProtocol à l'algorithme de jeux) --> D((D))
```

***Graphique d'initialisation de connection***
```mermaid
graph TD;

    C((C)) --> does_protocol_has_game_id{La trame possède-t'elle <br />un id de partie ?}
    does_protocol_has_game_id -->|Oui| controller_search_game_with_id{y-as-t'il une partie privé <br/>avec un id similaire ?}
    controller_search_game_with_id -->|Oui| controller_join_game(Rejoint la partie et préviens les deux joueurs <br/>que la partie débute.) --> E((E))
    controller_search_game_with_id -->|Non| controller_create_private_game(Créer une partie privé avec cette id et <br/>signale au joueur qu'il est en attente d'un deuxième joueur) --> E

    does_protocol_has_game_id -->|Non| controller_search_game_without_id{Y-as-t'il une partie <br/>publique disponible ?}
    controller_search_game_without_id -->|Oui| controller_join_game
    controller_search_game_without_id -->|Non| controller_create_public_game(Créer une partie publique <br/>avec cette id et signale au joueur <br/>qu'il est en attente d'un deuxième joueur) --> E
```

***Graphique de jeux***
```mermaid
graph TD;
    D((D)) --> controller_playing_ok{La partie est en cours. <br />Les deux joueurs ont-ils joué ?}

    controller_playing_ok -->|Oui| controller_playing_ok_result(Calcule le résultat du round, <br />envoie le résultat au deux joueurs)
    controller_playing_ok_result --> controller_playing_ok_result_next_round{La partie as-t'elle atteint <br />la limite de round ?}

    controller_playing_ok_result_next_round -->|Oui| controller_playing_ok_result_next_finish(Attend la deconnexion de <br />l'un des joueurs) --> E((E))
    controller_playing_ok_result_next_round -->|Non| controller_playing_ok_result_next(Passe au round suivant et réinitialise <br/>les actions des deux joueurs pour le prochain round.) --> E

    controller_playing_ok -->|Non| E
```

## Gestions des connections

Lors d'une nouvelle connexion le serveur créer et ajoute une structure Connection dans un tableau globale. Cela est faits à des fin d'accès, ces connexions ne sont pas directement modifié ou utilisé dans le programme.

Lors de la création d'une nouvelle partie, la structure Game correspondante possèdera deux liens vers les connexions des deux joueurs correspondants. Ce sont ces liens qui sont utilisés tout au long du traitement de la partie.

## Gestions des parties

Toutes les parties sont stockés dans la variable Stack global games_array du controller. De base toutes les parties sont initialisées à 0. Leurs status progresse au fur et à mesure de l'avencement d'une partie. Lors de la création d'une partie, elle récupère le status GAME_STATUS_PLAYING_OK. Si un joueur envoie une action et que l'autre joueur n'as pas encore envoyé sa réponse, la partie aura le status GAME_STATUS_PLAYING_WAITING. Une fois les deux actions des deux joueurs reçus, le status de la partie redevient GAME_STATUS_PLAYING_OK.

Une particularité des structure Game est que les joueurs sont représenté par des structure Connection. Ces structures ne sont en réalité que des liens vers les connections stockés dans la variable global connections_array du fichier `connection.c`

Une manière de déterminer si un joueur est présent est de tester son `sockfd`. Si celui-ci est égale à 0 cela signifie qu'aucun joueur n'est connecté.

## Gestions des logs

Les logs sont gérer via une surcouche des parties. Chaque fichier de logs est initialisé à la création d'une partie. L'historisation n'est cependant pas automatique, une fonction `logs_write`dédié à été créer et celle-ci est appelé dès qu'une action sur une partie nécessite une historisation.

Chaque ligne de logs contient la date et l'heure précise à laquel l'action à été enregistré, ainsi qu'un status et un message de détail supplémentaire. Des messages de logs additionel du serveur sont générer sur le flux du terminale mais ne sont cependant pas sauvegardé dans un fichier.
